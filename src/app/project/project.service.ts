import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Subject } from 'rxjs';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class ProjectService { 
  userBaseUrl='http://staging1.delivery-projects.com/dar-app-api/api/v1/user';
  accountBaseURl='http://staging1.delivery-projects.com/dar-app-api/api/v1/account';
  projectBaseUrl='http://staging1.delivery-projects.com/dar-app-api/api/v1/project';
  baseUrl='http://staging1.delivery-projects.com/dar-app-api/api/v1/';
  projects =new Subject();
  isPopUpopen=new Subject<any>();
  constructor(private _http:HttpClient) { }
  getUserList(){

    //  let designation=JSON.parse(localStorage.getItem("currentUser")).user.user[0].designation;
    //  return this._http.post(`${this.userBaseUrl}/get-all-users/`,{designation})
      return this._http.get(`${environment.apiBaseUrl}/user/get-all-manager`);

  }
  getAllUserList(){
    return this._http.get(`${environment.apiBaseUrl}/user/get-users`);
  }
  getAccounts(){
    return this._http.get(`${environment.apiBaseUrl}/account/get-account`)
  }
  addProject(project){
      return this._http.post(`${environment.apiBaseUrl}/project/create-project`,{project})
  }
  getProjectsByProjectManager(id){
      
       return this._http.get(`${environment.apiBaseUrl}/project/get-projects/${id}`);
  }
  getAllProject(){
    return this._http.get(`${environment.apiBaseUrl}/project/get-all-projects`);
  }
  getAssignedUserOfProject(project_id){
    return this._http.get(`${environment.apiBaseUrl}/project/get-assigned-users/${project_id}`);

  }
  assignProjectToEmp(data){
    return this._http.post(`${environment.apiBaseUrl}/project/assigned`,{data})
  }
  removeAssignUser(data){
    return this._http.post(`${environment.apiBaseUrl}/project/delete-emp`,{user:data})
  }
  updateProject(data){
    return this._http.put(`${environment.apiBaseUrl}/project/edit-project`,{project:data})
  }
  getBrand(id){
     return this._http.get(`${environment.apiBaseUrl}/account/get-brand/${id}`);
  }
  getCategories(){
    return this._http.get(`${environment.apiBaseUrl}/account/get-category`);
  }
  getSubCategories(id){
    return this._http.get(`${environment.apiBaseUrl}/account/get-sub-category/${id}`);
  }
  getProjectSource(){
      return this._http.get(`${environment.apiBaseUrl}/project/project-source`);
  }
  getActivities(){
    return this._http.get(`${environment.apiBaseUrl}/project/get-activity`)
  }
  getTeams(){
    return this._http.get(`${environment.apiBaseUrl}/team/get-teams`)
  }
  getAccountManager(id){
    return this._http.get(`${environment.apiBaseUrl}/team/get-account-manager/${id}`)
  }
  getSingleProject(id){
    return this._http.get(`${environment.apiBaseUrl}/project/get-project/${id}`)
  }
  getSubBrand(brandID){
    return this._http.get(`${environment.apiBaseUrl}/brand/get-sub-brand/${brandID}`)
  }
  getLanguage(){
    return this._http.get(`${environment.apiBaseUrl}/project/get-language`);
  }
  getClientManager(){
    return this._http.get(`${environment.apiBaseUrl}/project/get-client-manager`);
  }
}
