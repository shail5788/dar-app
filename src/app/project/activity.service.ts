import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import {HttpClient} from "@angular/common/http"
@Injectable({
  providedIn: 'root'
})
export class ActivityService {
  isAssignPopUpopen=new Subject<any>()
  baseUrl="http://staging1.delivery-projects.com/dar-app-api//api/v1";
  constructor(private _http:HttpClient) { }
  assignActivity(activity){
      return this._http.post(`${this.baseUrl}/activity/assigned-activity`,{data:activity})
  }
  getAssignedUserOfActivity(activity_id){
    return  this._http.get(`${this.baseUrl}/activity/get-assigned-users/${activity_id}`);
  }
  removeAssignUser(item){
    return this._http.post(`${this.baseUrl}/activity/delete-activity-emp`,{user:item})
  }
}
