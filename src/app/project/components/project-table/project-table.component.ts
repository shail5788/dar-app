import { Component, OnInit,Input } from '@angular/core';
import {DesignUtilityService} from '../../../share-module/services/design-utility.service';
import {ProjectService} from "../../project.service"
import { ProjectEditModalComponent } from '../project-edit-modal/project-edit-modal.component';
@Component({
  selector: 'app-project-table',
  templateUrl: './project-table.component.html',
  styleUrls: ['./project-table.component.css']
})
export class ProjectTableComponent implements OnInit {
  @Input() projects;
  isProjectAssignModel;
  isProjectEditModal;
  projectData;
  title:any;
  projectList;
  constructor(private _designUtility:DesignUtilityService,private _projectService:ProjectService) { 
     this._projectService.projects.subscribe(res=>{
         this.projectList=res;
         console.log(this.projectList)
     })  
  }
  ngOnInit(): void {
     this.isProjectAssignModel=false;
     this.projectList=this.projects
    
  }
  assignModal(project){
   
    this.projectData=project;
    this.isProjectAssignModel=true;
    this._designUtility.projectAssignModel.next(true)
    this._projectService.isPopUpopen.next(false);
   
  }
  editModal(project){
   
    this.projectData=project;
    this.isProjectEditModal=true;
    this._designUtility.projectEditModal.next(true)
  }
  searchByTitle(){
    console.log(this.title)
    if(this.title==""){
      this.ngOnInit();
    }else{
      this.projectList= this.projectList.filter(res=>{return (res.project_title.toLocaleLowerCase().match(this.title.toLocaleLowerCase())|| res.account_name.toLocaleLowerCase().match(this.title.toLocaleLowerCase()))})
    }
  }
}
