import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import{DesignUtilityService} from '../../../share-module/services/design-utility.service'
import {ProjectService} from '../../project.service';
import {BrandModel} from '../../../models/brand.model'
import {CategoryModel} from '../../../models/category.model';
import {SubCategoryModel} from '../../../models/subCategory.model';
import { ToastrManager } from "ng6-toastr-notifications";
@Component({
  selector: 'app-project-modal-popup',
  templateUrl: './project-modal-popup.component.html',
  styleUrls: ['./project-modal-popup.component.css']
})
export class ProjectModalPopupComponent implements OnInit {
  isModelClose=false;
  userList;
  users;
  accountData;
  accounts;
  projectData;
  brandData;
  categoryData;
  subCategoryData;
  categories:CategoryModel[]=[];
  subCategories:SubCategoryModel[]=[];
  brands:BrandModel[]=[];
  loading=true;
  loadingText;
  projectSource;
  projectSourceData;
  teamData;
  teams;
  accountManagerData;
  accountManager;
  constructor(private _designUtility:DesignUtilityService,private _projectService:ProjectService,public _toastr:ToastrManager) { 
     
  }

  preojectReactiveForm:FormGroup; 
  tasks:FormGroup;
  ngOnInit(): void {
    this.loadingText="Save Project";
    this.preojectReactiveForm=new FormGroup({
       'project_title':new FormControl(null,Validators.required),
       'team_type':new FormControl(null,Validators.required),
       'description':new FormControl(null),
       
       'tasks':new FormArray([this.inIntTaskRow()]),

       'account_id':new FormControl(null,Validators.required),
       'brand_id':new FormControl(null,Validators.required),
       'sub_brand_id':new FormControl(null),
       'managed_by':new FormControl(null,Validators.required),
       'projectStatus':new FormControl(null,Validators.required),
       'startDate':new FormControl(null,Validators.required),
       'endDate':new FormControl(null,Validators.required),
       'jira_no':new FormControl(null),
       'cradle':new FormControl(null),
       'client_manager_name':new FormControl(null,Validators.required),
       'po':new FormControl(null),
       'currency_name':new FormControl(null),
       'costing':new FormControl(null,Validators.pattern(/^-?(0|[1-9]\d*)?$/))
       
    })

    this.getTeams();
    this.getUser();
    this.getAccount();
    this.getCategory();
    this.getProjectSource();
  }
  inIntTaskRow(){
    return this.tasks=new FormGroup({
       'project_type':new FormControl(null,Validators.required),
       'sub_cate':new FormControl(null,Validators.required),
       'quantity':new FormControl(null,Validators.required),
       'language':new FormControl(null,Validators.required),
    })
    
 
  }
  getUser(){
      this._projectService.getUserList().subscribe(res=>{
        this.userList=res;
        this.users=this.userList.result.user;
      },err=>{
        console.log(err)
      }) 
  }
  getAccount(){
    this._projectService.getAccounts().subscribe(res=>{
      this.accountData=res;
      this.accounts=this.accountData.result.account;
    },err=>{
      console.log(err)
    }) 
}
  closeModel(){
    this.isModelClose=true;
    this._designUtility.projectPopUp.next(false)
  }
  addProject(){
      // console.log(this.preojectReactiveForm.value)
      this.loadingText="Please Wait...";
      //console.log(this.accountManager)
       let project={...this.preojectReactiveForm.value}
       let user =JSON.parse(localStorage.getItem("currentUser")).user.user[0];
       
       project.account_manager_id=this.accountManager[0].account_manager;
      //  console.log(project);
       this._projectService.addProject(project).subscribe(res=>{
       this.projectData=res;   
       this.getAllProject();
       this.loading=false;
       this._toastr.successToastr("Project has been added successfully!","Success")
       this.closeModel()
      },err=>{
        console.log(err)
        this._toastr.errorToastr(err.error.result.errors.error)
      })
  }
  getAllProject(){
    let user =JSON.parse(localStorage.getItem("currentUser")).user.user[0];
    if(user.designation!="vp" && user.designation!="avp"){
        this._projectService.getProjectsByProjectManager(user.id).subscribe(res=>{
           this.projectData=res;
           this._projectService.projects.next(this.projectData.result);
        },err=>{})  
    }else{
        this._projectService.getAllProject().subscribe(res=>{
             this.projectData=res;
             this._projectService.projects.next(this.projectData.result);
        },err=>{console.log(err)})
    }
  }
  accountChange(accountID){
     this.getBrands(accountID)
  }
  teamSelect(teamID){
    // console.log(teamID)
     this.getAccountManangerAssociatedWithTeam(teamID)
  }
  getBrands(accountID){
      this._projectService.getBrand(accountID).subscribe(
        res=>{
        
          this.brandData=res;
          this.brands=this.brandData.result;
          console.log(this.brands)
        },
        err=>{console.log(err)})

  }
  getCategory(){
     this._projectService.getCategories().subscribe(
       res=>{
         this.categoryData=res;
         this.categories=this.categoryData.result;
         console.log( this.categories)
        },
      err=>{
        console.log(err)
      })   
  }
  changeCategory(cateId){
    
    this.getSubCategories(cateId)
  }
  getSubCategories(id){
      this._projectService.getSubCategories(id).subscribe(
        res=>{
          console.log(res)
          this.subCategoryData=res;
          this.subCategories=this.subCategoryData.result;
        },
        err=>{
          console.log(err)
        })
  }
  getProjectSource(){
      this._projectService.getProjectSource().subscribe(res=>{
          this.projectSourceData=res;
          this.projectSource=this.projectSourceData.result; 
      },err=>{})
  }

  getTeams(){
    this._projectService.getTeams().subscribe(
        res=>{
          this.teamData=res;
          this.teams=this.teamData.result
        },
        err=>{console.log(err)}
     )
  }
  getAccountManangerAssociatedWithTeam(id){
     this._projectService.getAccountManager(id).subscribe(
      res=>{this.accountManagerData=res; this.accountManager=this.accountManagerData.result},
      err=>{}
      )
  }
  addMore(){
   let forArr=  this.preojectReactiveForm.get('tasks') as FormArray;
     forArr.push(this.inIntTaskRow())
  }
}
