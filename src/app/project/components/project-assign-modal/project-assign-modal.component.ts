
import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms'
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import {DesignUtilityService}from '../../../share-module/services/design-utility.service';
import {ProjectService} from '../../project.service'
import { ToastrManager } from "ng6-toastr-notifications";
@Component({
  selector: 'app-project-assign-modal',
  templateUrl: './project-assign-modal.component.html',
  styleUrls: ['./project-assign-modal.component.css']
})
export class ProjectAssignModalComponent implements OnInit,OnChanges {
  isModelClose=false;
  assignReactiveForm:FormGroup
  @Input() projectData;
  dropdownList = [];
  selectedItems = [];
  assignedUser;
  users;
  loadingText;
  dropdownSettings:IDropdownSettings;
  constructor(private _designService:DesignUtilityService,private _projectService:ProjectService,public toastr:ToastrManager) {
     
      this._designService.projectAssignModel.subscribe(res=>{
        console.log(res);
        this.isModelClose=!res;
      })
   }

   ngOnChanges(changes:SimpleChanges){
      console.log(changes);
     
      this.assignedUser=[];
      this._projectService.getAssignedUserOfProject(this.projectData.id).subscribe(res=>{
        this.assignedUser=res;
      
        console.log("this.assignedUser",this.assignedUser)
        if(this.assignedUser.result.length>0){
          console.log("SDf")
          this.selectedItems=this.assignedUser.result;
          console.log(this.selectedItems)
        }else{
          this.selectedItems=[]
        }
      },err=>{})
   }

  ngOnInit(): void {
    this.loadingText="Assign";
      this._projectService.getAllUserList().subscribe(res=>{
          this.users =res;
          let userData=[];
          this.users.result.user.forEach(element => {
              element.user_id=element.id
              delete element.id;
              userData.push(element)
          });
             
       this.dropdownList=userData;//this.users.result;
     },err=>{console.log(err)})
     this._projectService.getAssignedUserOfProject(this.projectData.id).subscribe(res=>{
        this.assignedUser=res;
       
        if(this.assignedUser.result.length>0){
          this.selectedItems=this.assignedUser.result;
        }else{
          this.selectedItems=[]
        }
        console.log(this.selectedItems)
     },err=>{})
    
    
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'user_id',
      textField: 'name',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 5,
      allowSearchFilter: true
    };

   this.assignReactiveForm=new FormGroup({
        'user_id':new FormControl(null,Validators.required)
    })
    
  }
 closeModel(){
    this.isModelClose=true
    this._designService.projectAssignModel.next(false)
  }
  assigningProject(){
    this.loadingText="Please wait...";
    let empList=this.assignReactiveForm.value;
      let assigned_emp=[];
       empList.user_id.forEach(emp => {
          emp.project_id=this.projectData.id;
          emp.assigned_by=this.projectData.managed_by;
          delete emp.name;
         assigned_emp.push(emp)
       });
      console.log(assigned_emp)

      this._projectService.assignProjectToEmp(assigned_emp).subscribe(res=>{
        this.loadingText="Assign";
         this.toastr.successToastr("Project assigned successfully!" ,"Success")
      },err=>{
        this.toastr.successToastr("Something went wrong!" ,"Error")
      })
  }
  onItemSelect(item: any) {
    //console.log(item);
  }
  onSelectAll(items: any) {
    //console.log(items);
  }
  onItemDeSelect (item:any){
    item.project_id=this.projectData.id;
    this._projectService.removeAssignUser(item).subscribe(res=>{
       console.log(res)
       this.toastr.successToastr("Employee removed!","Success")
    },err=>{console.log(err)
      this.toastr.errorToastr("something went wrong","Error")
    })
  }

}
