import { Component, OnInit ,Input} from '@angular/core';
import { DesignUtilityService } from 'src/app/share-module/services/design-utility.service';
import { ActivityService } from '../../activity.service';
import { ProjectService } from '../../project.service';
import { Activity } from './../../../models/activity.model'

@Component({
  selector: 'app-activity-list',
  templateUrl: './activity-list.component.html',
  styleUrls: ['./activity-list.component.css']
})
export class ActivityListComponent implements OnInit {
  @Input() activities:Activity;
  activityData//: Activity;
  isActivityModalOPen=false;
  private _projectService: any;
  constructor(private _disignService:DesignUtilityService,
    private _projectServic:ProjectService,
    private _activeService:ActivityService) { }

  ngOnInit(): void {
    console.log(this.activities)
  }
  openActivityModel(){
      
  }
  editModal(activity){}
  assignModal(activity){
    this.isActivityModalOPen=true;
    this.activityData=activity;
    this._disignService.activityAssignModel.next(true)
    //this._activeService.isAssignPopUpopen.next(false);
   // console.log(this.activityData)
  }
}
