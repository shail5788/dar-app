import { Component, OnInit } from '@angular/core';
import { Activity } from 'src/app/models/activity.model';
import { ProjectService } from '../../project.service';

@Component({
  selector: 'app-activity-management',
  templateUrl: './activity-management.component.html',
  styleUrls: ['./activity-management.component.css']
})
export class ActivityManagementComponent implements OnInit {
   activityData;
   activities:Activity;
  constructor(private _projectService:ProjectService) { }

  ngOnInit(): void {
    this.getAllActivities();
  }
  openActivityModel(){

  }
  getAllActivities(){
    this._projectService.getActivities().subscribe(
      res=>{
        this.activityData=res;
        this.activities=this.activityData.result;
      },
      err=>{console.log(err)}
      )
  }
}
