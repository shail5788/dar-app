import { Component, OnInit } from '@angular/core';
import {DesignUtilityService} from '../../../share-module/services/design-utility.service'
import {ProjectService} from '../../project.service';
@Component({
  selector: 'app-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.css']
})
export class ProjectListComponent implements OnInit {
  isProjectModal=false;
  projectData;
  projects;
  user;
  constructor(private _designUtility:DesignUtilityService,private _projectService:ProjectService) { 
     this._designUtility.projectPopUp.subscribe(res=>{
        this.isProjectModal=res;
     })
     this.user =JSON.parse(localStorage.getItem("currentUser")).user.user[0];
     this._projectService.projects.subscribe(res=>{
      //  console.log("callled")
      //  console.log(res);
      if(this.user.designation!="vp" && this.user.designation!='avp'){
        this.getProjectsByProjectManager()
      }else{
        this.getAllProject()
      }
       
     })
  }

  ngOnInit(): void {
    if(this.user.designation!="vp" && this.user.designation!='avp'){
      this.getProjectsByProjectManager()
    }else{
      this.getAllProject()
    }
  }

  getProjectsByProjectManager(){
    let id =JSON.parse(localStorage.getItem("currentUser")).user.user[0].id;
    this._projectService.getProjectsByProjectManager(id).subscribe(res=>{
          this.projectData=res;
          this.projects=this.projectData.result;       
          // console.log(this.projectData.result)
    },err=>{console.log(err)})
  }
  getAllProject(){
    this._projectService.getAllProject().subscribe(res=>{
      this.projectData=res;
      this.projects=this.projectData.result;       
      // console.log(this.projectData.result)
      },err=>{console.log(err)})
  }
 
  openProjectModel(){
    
    this._designUtility.projectPopUp.next(true) 
  }
 


}
