import { Component, OnInit,Input, OnChanges } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { DesignUtilityService } from 'src/app/share-module/services/design-utility.service';
import { ToastrManager } from "ng6-toastr-notifications";
import { ProjectService } from '../../project.service';
import {BrandModel} from '../../../models/brand.model'
import {CategoryModel} from '../../../models/category.model';
import {SubCategoryModel} from '../../../models/subCategory.model';
@Component({
  selector: 'app-project-edit-modal',
  templateUrl: './project-edit-modal.component.html',
  styleUrls: ['./project-edit-modal.component.css']
})
export class ProjectEditModalComponent implements OnInit,OnChanges {
   @Input()projectData; 
  isModelClose=false;
    userList;
    users;
    accountData;
    accounts;
    projects;
    // projectData;
    brandData;
    categoryData;
    subCategoryData;
    categories:CategoryModel[]=[];
    subCategories:SubCategoryModel[]=[];
    brands:BrandModel[]=[];
    loadingText;
    projectSource;
    projectSourceData;
    accountManagerData;
  accountManager;
  teamData;
  teams;
    constructor(private _designUtility:DesignUtilityService,private _projectService:ProjectService,public _toastr:ToastrManager) { 
        this._designUtility.projectEditModal.subscribe(res=>{
          this.isModelClose=!res;
        })
    }

    preojectReactiveForm:FormGroup; 
        ngOnInit(): void {
          this.loadingText="Update Project";
          let endDate=this.projectData.endDate;
          let startDate=this.projectData.startDate;
          this.projectData.endDate=new Date(endDate)
          this.projectData.startDate=new Date(startDate)
          this.preojectReactiveForm=new FormGroup({
            'project_title':new FormControl(null,Validators.required),
            'team_type':new FormControl(null,Validators.required),
            'description':new FormControl(null),
            'project_type':new FormControl(null,Validators.required),
            'sub_cate':new FormControl(null,Validators.required),
            'account_id':new FormControl(null,Validators.required),
            'brand_id':new FormControl(null,Validators.required),
            'managed_by':new FormControl(null,Validators.required),
            'projectStatus':new FormControl(null,Validators.required),
            'startDate':new FormControl(null,Validators.required),
            'endDate':new FormControl(null,Validators.required),
            'project_source':new FormControl(null,Validators.required),
            'client_manager_name':new FormControl(null,Validators.required),
            'costing':new FormControl(null,Validators.pattern(/^-?(0|[1-9]\d*)?$/))
            
          })
          this.getTeams()
          this.getUser()
          this.getAccount();
          this.getCategory();
          this.getProjectSource();
          console.log(this.projectData)
          //this.getBrands(this.projectData.account_id)
          this.preojectReactiveForm.patchValue(this.projectData)
        }
        ngOnChanges(){
          this.getBrands(this.projectData.account_id)
          this.getSubCategories(this.projectData.project_type);
          if(typeof this.preojectReactiveForm!=undefined && typeof this.preojectReactiveForm!="undefined"){
            let endDate=this.projectData.endDate;
            let startDate=this.projectData.startDate;
            this.projectData.endDate=new Date(endDate)
            this.projectData.startDate=new Date(startDate)
            this.preojectReactiveForm.patchValue(this.projectData)
          }
         
        }
        getUser(){
            this._projectService.getUserList().subscribe(res=>{
              this.userList=res;
              this.users=this.userList.result.user;
            },err=>{
              console.log(err)
            }) 
        }
        getAccount(){
          this._projectService.getAccounts().subscribe(res=>{
            this.accountData=res;
            this.accounts=this.accountData.result.account;
          },err=>{
            console.log(err)
          }) 
      }
    closeModel(){
     
      this.isModelClose=true;
      this._designUtility.projectAssignModel.next(false)
    }
    updateProject(){
         this.loadingText="Please Wait...";
         let payload={...this.preojectReactiveForm.value}
         payload.id=this.projectData.id;
         this._projectService.updateProject(payload).subscribe(
          res=>{
          console.log(res);
          this.loadingText="Update Project";
            this._toastr.successToastr("Project updated successfully !","Success")
            this.getProjectsByProjectManager()
            this.closeModel();
        },
          err=>{
            console.log(err)
          }
       )
    }
    getProjectsByProjectManager(){
      let user =JSON.parse(localStorage.getItem("currentUser")).user.user[0];
      if(user.designation!="vp" && user.designation!="avp"){
        this._projectService.getProjectsByProjectManager(user.id).subscribe(res=>{
          this.projects=res; 
          this._projectService.projects.next(this.projects.result)
        },err=>{console.log(err)})
      }else{
        this._projectService.getAllProject().subscribe(res=>{
          this.projectData=res;
          this._projectService.projects.next(this.projectData.result);
         },err=>{console.log(err)})
      }
   
    }
    accountChange(accountID){
      this.getBrands(accountID)
   }
   teamSelect(teamID){
    // console.log(teamID)
     this.getAccountManangerAssociatedWithTeam(teamID)
  }
   getBrands(accountID){
       this._projectService.getBrand(accountID).subscribe(
         res=>{
         
           this.brandData=res;
           this.brands=this.brandData.result;
           console.log(this.brands)
         },
         err=>{console.log(err)})
 
   }
   getCategory(){
      this._projectService.getCategories().subscribe(
        res=>{
          this.categoryData=res;
          this.categories=this.categoryData.result;
          console.log( this.categories)
         },
       err=>{
         console.log(err)
       })   
   }
   changeCategory(cateId){
     
     this.getSubCategories(cateId)
   }
   getSubCategories(id){
       this._projectService.getSubCategories(id).subscribe(
         res=>{
           console.log(res)
           this.subCategoryData=res;
           this.subCategories=this.subCategoryData.result;
         },
         err=>{
           console.log(err)
         })
   }
   getProjectSource(){
      this._projectService.getProjectSource().subscribe(res=>{
          this.projectSourceData=res;
          this.projectSource=this.projectSourceData.result; 
      },err=>{})
   }
   getTeams(){
    this._projectService.getTeams().subscribe(
        res=>{
          this.teamData=res;
          this.teams=this.teamData.result
        },
        err=>{console.log(err)}
     )
  }
  getAccountManangerAssociatedWithTeam(id){
     this._projectService.getAccountManager(id).subscribe(
      res=>{this.accountManagerData=res; this.accountManager=this.accountManagerData.result},
      err=>{}
      )
  }
}
