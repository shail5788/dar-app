import { Component, OnInit,Input, SimpleChanges,OnChanges } from '@angular/core';
import {FormGroup,FormControl,Validators}from '@angular/forms';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import {DesignUtilityService}from '../../../share-module/services/design-utility.service';
import {ProjectService} from '../../project.service'
import { ToastrManager } from "ng6-toastr-notifications";
import { ActivityService } from '../../activity.service';
@Component({
  selector: 'app-activity-assign-modal',
  templateUrl: './activity-assign-modal.component.html',
  styleUrls: ['./activity-assign-modal.component.css']
})
export class ActivityAssignModalComponent implements OnInit ,OnChanges{
  @Input() activity;
  isModelClose=false;
  activityAssignReactiveForm:FormGroup;
  dropdownList = [];
  selectedItems = [];
  assignedUser;
  users;
  loadingText;
  dropdownSettings:IDropdownSettings;
  constructor(private _designService:DesignUtilityService,  
               private _projectService:ProjectService,
               public toastr:ToastrManager,
               private _activityService:ActivityService) {
     
      this._designService.activityAssignModel.subscribe(res=>{
        console.log(res);
        this.isModelClose=!res;
      })
   }
   ngOnChanges(changes:SimpleChanges){
    console.log(changes);
    
   
    this.assignedUser=[];
    this._activityService.getAssignedUserOfActivity(this.activity.id).subscribe(res=>{
      this.assignedUser=res;
    
      console.log("this.assignedUser",this.assignedUser)
      if(this.assignedUser.result.length>0){
        console.log("SDf")
        this.selectedItems=this.assignedUser.result;
        console.log(this.selectedItems)
      }else{
        this.selectedItems=[]
      }
    },err=>{})
 }
  ngOnInit(): void {
    this.loadingText="Assign";
    this._projectService.getAllUserList().subscribe(res=>{
        this.users =res;
        let userData=[];
        this.users.result.user.forEach(element => {
            element.user_id=element.id
            delete element.id;
            userData.push(element)
        });
           
     this.dropdownList=userData;//this.users.result;
   },err=>{console.log(err)})

   this._activityService.getAssignedUserOfActivity(this.activity.id).subscribe(res=>{
    this.assignedUser=res;
   
          if(this.assignedUser.result.length>0){
            this.selectedItems=this.assignedUser.result;
          }else{
            this.selectedItems=[]
          }
          console.log(this.selectedItems)
      },err=>{})  


      this.dropdownSettings = {
        singleSelection: false,
        idField: 'user_id',
        textField: 'name',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        itemsShowLimit: 10,
        allowSearchFilter: true
      };

    this.activityAssignReactiveForm=new FormGroup({
      'user_id':new FormControl(null,Validators.required)
    })

    
  }
  closeModel(){
    this.isModelClose=true
    this._designService.activityAssignModel.next(false)
    
  }

  onItemSelect(item: any) {
    //console.log(item);
  }
  onSelectAll(items: any) {
    //console.log(items);
  }
  onItemDeSelect (item:any){
    item.activity_id=this.activity.id;
    this._activityService.removeAssignUser(item).subscribe(res=>{
       console.log(res)
       this.toastr.successToastr("Employee removed!","Success")
    },err=>{console.log(err)
      this.toastr.errorToastr("something went wrong","Error")
    })
  }
  assigningActivity(){
    this.loadingText="Please wait...";
    let empList=this.activityAssignReactiveForm.value;
      let assigned_emp=[];
       empList.user_id.forEach(emp => {
          emp.activity_id=this.activity.id;
          emp.created_by=this.activity.created_by;
          delete emp.name;
         assigned_emp.push(emp)
       });
       console.log(assigned_emp)
      this._activityService.assignActivity(assigned_emp).subscribe(
        res=>{console.log(res)},
        err=>{console.log(err)}
        )
  }

}
