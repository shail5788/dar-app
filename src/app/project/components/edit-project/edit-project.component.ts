import { Component, OnInit,Input, OnChanges } from '@angular/core';
import { FormArray, FormControl,FormBuilder ,FormGroup, Validators } from '@angular/forms';
import { DesignUtilityService } from 'src/app/share-module/services/design-utility.service';
import { ToastrManager } from "ng6-toastr-notifications";
import { ProjectService } from '../../project.service';
import {BrandModel} from '../../../models/brand.model';
import {subBrandModel} from '../../../models/subBrand..model';
import {CategoryModel} from '../../../models/category.model';
import {SubCategoryModel} from '../../../models/subCategory.model';
import { ActivatedRoute } from '@angular/router';
import {LanguageModel} from './../../../models/language.model'
import { ClientManager } from '../../../models/clientManager.model';
@Component({
  selector: 'app-edit-project',
  templateUrl: './edit-project.component.html',
  styleUrls: ['./edit-project.component.css']
})
export class EditProjectComponent implements OnInit {
  userList;
  users;
  accountData;
  accounts;
  projectData;
  brandData;
  subBrand:subBrandModel[]=[];
  subBrandData;
  categoryData;
  subCategoryData;
  categories:CategoryModel[]=[];
  subCategories:SubCategoryModel[]=[];
  subCategory:any=[];
  brands:BrandModel[]=[];
  loading=true;
  loadingText;
  projectSource;
  projectSourceData;
  teamData;
  teams;
  accountManagerData;
  accountManager;
  isModelClose: boolean;
  projectId;
  project;
  projects;
  lang;
  langauages:LanguageModel[]=[];
  qunatities=[];
  cmanager;
  clientManagers:ClientManager[]=[]
  constructor(private _designUtility:DesignUtilityService,private _projectService:ProjectService,public _toastr:ToastrManager,private activatedRoute:ActivatedRoute,private fb:FormBuilder) { 
     this.activatedRoute.params.subscribe(data=>{
       this.projectId=data.id;
       console.log(this.projectId)
     })
  }

  preojectReactiveForm:FormGroup; 
  tasks:FormGroup;
  ngOnInit(): void {
    this.loadingText="Update Project";
    for(let i=1;i<100;i++){this.qunatities.push(i)}
    this.getProject(this.projectId);  
    this.preojectReactiveForm=new FormGroup({
       'project_title':new FormControl(null,Validators.required),
       'team_type':new FormControl(null,Validators.required),
       'description':new FormControl(null),
       
       'tasks':new FormArray([this.inIntTaskRow(null)]),

       'account_id':new FormControl(null,Validators.required),
       'brand_id':new FormControl(null,Validators.required),
       'sub_brand_id':new FormControl(null),
       'managed_by':new FormControl(null,Validators.required),
       'projectStatus':new FormControl(null,Validators.required),
       'startDate':new FormControl(null,Validators.required),
       'endDate':new FormControl(null,Validators.required),
       'jira_no':new FormControl(null),
       'cradle':new FormControl(null),
       'client_manager_name':new FormControl(null,Validators.required),
       'po':new FormControl(null),
       'currency_name':new FormControl(null),
       'costing':new FormControl(null,Validators.pattern(/^-?(0|[1-9]\d*)?$/))
       
    })

    this.getTeams();
    this.getUser();
    this.getAccount();
    this.getCategory();
    this.getProjectSource();
    this.getLanguage();
    this.getClientManager();
   // console.log(this.project)
    
  
  }
  inIntTaskRow(data):FormGroup{
    data={project_type:"",su_cate:"",quantity:"",language:""}
    // return this.tasks=new FormGroup({
    //    'project_type':new FormControl(null,Validators.required),
    //    'sub_cate':new FormControl(null,Validators.required),
    //    'quantity':new FormControl(null,Validators.required),
    //    'language':new FormControl(null,Validators.required),
    // })
    return this.tasks=this.fb.group({

        'project_type':[data.project_type,Validators.required],
        'sub_cate':[data.sub_cate,Validators.required],
        'quantity':[data.quantity,Validators.required],
        'language':[data.language,Validators.required]
    })
    
 
  }
  getUser(){
      this._projectService.getUserList().subscribe(res=>{
        this.userList=res;
        this.users=this.userList.result.user;
      },err=>{
        console.log(err)
      }) 
  }
  getAccount(){
    this._projectService.getAccounts().subscribe(res=>{
      this.accountData=res;
      this.accounts=this.accountData.result.account;
    },err=>{
      console.log(err)
    }) 
}
getLanguage(){
  this._projectService.getLanguage().subscribe(
    res=>{this.lang=res;
      this.langauages=this.lang.result.languages;
    },
    err=>{}
    )
}
  closeModel(){
    this.isModelClose=true;
    this._designUtility.projectPopUp.next(false)
  }
  updateProject(){
  
      this.loadingText="Please Wait...";
      
      let payload={...this.preojectReactiveForm.value}
      payload.id=this.project.id;
      console.log(payload)
      this._projectService.updateProject(payload).subscribe(
       res=>{
       console.log(res);
       this.loadingText="Update Project";
         this._toastr.successToastr("Project updated successfully !","Success")
         this.getProjectsByProjectManager()
         //this.closeModel();
      },
       err=>{
         console.log(err)
       }
    )
  }
  getAllProject(){
    let user =JSON.parse(localStorage.getItem("currentUser")).user.user[0];
    if(user.designation!="vp" && user.designation!="avp"){
        this._projectService.getProjectsByProjectManager(user.id).subscribe(res=>{
           this.projectData=res;
           this._projectService.projects.next(this.projectData.result);
        },err=>{})  
    }else{
        this._projectService.getAllProject().subscribe(res=>{
             this.projectData=res;
             this._projectService.projects.next(this.projectData.result);
        },err=>{console.log(err)})
    }
  }
  accountChange(accountID){
     this.getBrands(accountID)
  }
  getSubBrandOfBrand(id){
    this.getSubBrand(id)
  }
  teamSelect(teamID){
    // console.log(teamID)
     this.getAccountManangerAssociatedWithTeam(teamID)
  }
  getBrands(accountID){
      this._projectService.getBrand(accountID).subscribe(
        res=>{
        
          this.brandData=res;
          this.brands=this.brandData.result;
        //  console.log(this.brands)
        },
        err=>{console.log(err)})

  }
  getCategory(){
     this._projectService.getCategories().subscribe(
       res=>{
         this.categoryData=res;
         this.categories=this.categoryData.result;
         //console.log( this.categories)
        },
      err=>{
        console.log(err)
      })   
  }
  changeCategory(cateId,i){
 
    this.getSubCategories(cateId,i)
  }
  getSubCategories(id,i){
      this._projectService.getSubCategories(id).subscribe(
        res=>{
        //  console.log(res)
          this.subCategoryData=res;
          this.subCategories=this.subCategoryData.result;
          this.subCategory[i]=this.subCategories;
        },
        err=>{
          console.log(err)
        })
  }
  getProjectSource(){
      this._projectService.getProjectSource().subscribe(res=>{
          this.projectSourceData=res;
          this.projectSource=this.projectSourceData.result; 
      },err=>{})
  }
  getClientManager(){
    this._projectService.getClientManager().subscribe(
      res=>{
        this.cmanager=res;
        this.clientManagers=this.cmanager.result;
      },
      err=>{console.log(err)}
      )
  }
  getTeams(){
    this._projectService.getTeams().subscribe(
        res=>{
          this.teamData=res;
          this.teams=this.teamData.result
        },
        err=>{console.log(err)}
     )
  }
  getAccountManangerAssociatedWithTeam(id){
     this._projectService.getAccountManager(id).subscribe(
      res=>{this.accountManagerData=res; this.accountManager=this.accountManagerData.result},
      err=>{}
      )
  }
  addMore(){
   let forArr=  this.preojectReactiveForm.get('tasks') as FormArray;
     forArr.push(this.inIntTaskRow(null))
  }
  remove(i){
    let forArr=  this.preojectReactiveForm.get('tasks') as FormArray;
    forArr.removeAt(i)
  }

  getProject(projectId){
      this._projectService.getSingleProject(projectId).subscribe(
        res=>{this.projectData=res;
          let forArr=  this.preojectReactiveForm.get('tasks') as FormArray;
          this.project=this.projectData.result[0];
          this.getSubBrand(this.project.brand_id)
          //console.log(this.project)
          let taskdata=[];
          this.getBrands(this.project.account_id)
          this.project.tasks.forEach((task,i) => {
              this.getSubCategories(task.project_type,i)
              delete task.id;
              delete task.project_id;
              taskdata.push(task)
              if(typeof forArr.at(i)==undefined || typeof forArr.at(i)=="undefined"){
                forArr.push(this.inIntTaskRow(task))
              
              }
          });
          console.log(taskdata)
          this.project.startDate=new Date(this.project.startDate)
          this.project.endDate=new Date(this.project.endDate)
          this.preojectReactiveForm.patchValue(this.project)
        },
        err=>{}
        )
  }
  getSubBrand(brandId){
      this._projectService.getSubBrand(brandId).subscribe(
        res=>{
          this.subBrandData=res;
          this.subBrand=this.subBrandData.result.sub_brand;
          console.log(this.subBrand)
        },
        err=>{console.log(err)})
  }
  getProjectsByProjectManager(){
    let user =JSON.parse(localStorage.getItem("currentUser")).user.user[0];
    if(user.designation!="vp" && user.designation!="avp"){
      this._projectService.getProjectsByProjectManager(user.id).subscribe(res=>{
        this.projects=res; 
        this._projectService.projects.next(this.projects.result)
      },err=>{console.log(err)})
    }else{
      this._projectService.getAllProject().subscribe(res=>{
        this.projectData=res;
        this._projectService.projects.next(this.projectData.result);
       },err=>{console.log(err)})
    }
 
  }

}
