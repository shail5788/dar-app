import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ActivityManagementComponent } from './components/activity-management/activity-management.component';
import { AddProjectComponent } from './components/add-project/add-project.component';
import { EditProjectComponent } from './components/edit-project/edit-project.component';
import { ProjectListComponent } from './components/project-list/project-list.component';


const routes: Routes = [
    {path:"",component:ProjectListComponent},
    {path:"add",component:AddProjectComponent},
    {path:"edit/:id",component:EditProjectComponent},
    {path:"activity-management",component:ActivityManagementComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProjectRoutingModule { }
