import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule,ReactiveFormsModule} from '@angular/forms';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ProjectRoutingModule } from './project-routing.module';
import { ProjectListComponent } from './components/project-list/project-list.component';
import { ProjectTableComponent } from './components/project-table/project-table.component';
import { ProjectModalPopupComponent } from './components/project-modal-popup/project-modal-popup.component';
import {ProjectService} from './project.service';
import { ProjectEditModalComponent } from './components/project-edit-modal/project-edit-modal.component';
import { ProjectAssignModalComponent } from './components/project-assign-modal/project-assign-modal.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { ActivityManagementComponent } from './components/activity-management/activity-management.component';
import { ActivityListComponent } from './components/activity-list/activity-list.component';
import {ActivityAssignModalComponent} from './components/activity-assign-modal/activity-assign-modal.component';
import { AddProjectComponent } from './components/add-project/add-project.component';
import { EditProjectComponent } from './components/edit-project/edit-project.component';
@NgModule({
  declarations: [ProjectListComponent, ProjectTableComponent, ProjectModalPopupComponent, ProjectEditModalComponent, ProjectAssignModalComponent, ActivityManagementComponent, ActivityListComponent,ActivityAssignModalComponent, AddProjectComponent, EditProjectComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    ProjectRoutingModule,
  
    BsDatepickerModule.forRoot(),
    NgMultiSelectDropDownModule.forRoot(),
    
  ],
  providers:[ProjectService]
})
export class ProjectModule { }
