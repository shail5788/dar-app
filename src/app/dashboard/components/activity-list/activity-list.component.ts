import { Component, OnInit,Input } from '@angular/core';
import { AccordionConfig } from 'ngx-bootstrap/accordion';
import { DashboardService } from '../../dashboard.service';

export function getAccordionConfig(): AccordionConfig {
  return Object.assign(new AccordionConfig(), { closeOthers: true });
}

@Component({
  selector: 'app-activity-list',
  templateUrl: './activity-list.component.html',
  styleUrls: ['./activity-list.component.css'],
  providers: [{ provide: AccordionConfig, useFactory: getAccordionConfig }]
})
export class ActivityListComponent implements OnInit {
  @Input() activityList;
  customClass="customClass";
  darWeakwiseDates=[];
  openProject;
  days=[];
  months=[];
  curr;
  constructor(private _dashboardService:DashboardService) { }
    
  ngOnInit(): void {
    this.days=['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
    this.months=['January',"Februray","March","April","May","June","July","Auguest","september","October","November","December"];    
    this.curr=new Date(); 
    this.darWeakwiseDates[0]=this.getCurrentWeekDates();
    this.darWeakwiseDates[1]=this.getLastWeekDates();
    //console.log(this.darWeakwiseDates);  
  }

  getCurrentWeekDates(){
      let currentWeek=[]
      let nextAndPrevDates:any={cweek:""};
      for (let i = 1; i <= 7; i++) {
          let calendarr={};
          let first = this.curr.getDate() - this.curr.getDay() + i
          let day = new Date(this.curr.setDate(first)).toISOString().slice(0, 10)
          let onlyDate=day.split("-")[2]
          calendarr['day']=this.days[this.curr.getDay()];
          calendarr['date']=parseInt(onlyDate);
          calendarr['month']=this.months[this.curr.getMonth()]
          calendarr['year']=this.curr.getFullYear().toString();
          currentWeek.push(calendarr);
          nextAndPrevDates.cweek=currentWeek;
        }
        return nextAndPrevDates;
  }
  getLastWeekDates(){
    let prevDates=[]
    let nextAndPrevDates:any={lastWeek:""};
    for(let j=0;j<=6;j++){
          let preDate={}
          var cur = new Date(); 
          var day = cur.getDate()-cur.getDay()-(6-j);
          var pdate = new Date(cur.setDate(day));
          preDate['day']=this.days[cur.getDay()]
          preDate['date']=parseInt(pdate.toISOString().slice(0,10).split("-")[2]);
          preDate['month']=this.months[cur.getMonth()]
          preDate['year']=cur.getFullYear().toString();
          prevDates.push(preDate)
          nextAndPrevDates.lastWeek=prevDates;
    }
    return nextAndPrevDates;
  }
  activeAccordion(project,event){
    console.log(event)
  
  }
  
  isOpenAccor(project){
    
   this.openProject=project;
   this._dashboardService.projectSub.next(this.openProject)
  }
   
}
