import { Component, OnInit,Input } from '@angular/core';
import { DarService } from '../../dar.service';
@Component({
  selector: 'app-deadline-notification',
  templateUrl: './deadline-notification.component.html',
  styleUrls: ['./deadline-notification.component.css']
})
export class DeadlineNotificationComponent implements OnInit {
    @Input() notifications;
    constructor(private _darService:DarService) { }

    ngOnInit(): void {
     // console.log(this.notifications)
    }

}
