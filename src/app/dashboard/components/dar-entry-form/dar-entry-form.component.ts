import { Component, Input, OnInit } from '@angular/core';
import {FormBuilder ,FormControl,FormGroup, Validators} from "@angular/forms";
import { ProjectStatus } from './../../../models/projectStatus.model';
import { DashboardService } from '../../dashboard.service';
import {TaskTypeModel} from './../../../models/taskTypeModel';
import { DarService } from '../../dar.service';


@Component({
  selector: 'app-dar-entry-form',
  templateUrl: './dar-entry-form.component.html',
  styleUrls: ['./dar-entry-form.component.css']
})
export class DarEntryFormComponent implements OnInit {
 @Input() darDates;
 @Input() project;
 @Input() activeProject;
 @Input() loopIndex;
 @Input() taskType:TaskTypeModel;
 @Input() projectStatus:ProjectStatus;
 twoWeeksDates;
 darEntryForm:FormGroup;
 DatePlaceholder;
 saveButton="Save";
 currentDar;
 selectedDateDar;
  constructor(private fb:FormBuilder,
    private _dashboardService:DashboardService,
    private _darService:DarService
    ) { }

  ngOnInit(): void {
    let reg = '(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?';
     this.getSortedDate(this.darDates)
    //   console.log(this.twoWeeksDates)
    let group ={};
     
           group["date-"+this.loopIndex]=['',Validators.required];
           group["task_type-"+this.loopIndex]=['',Validators.required];
           group["project_status-"+this.loopIndex]=['',Validators.required];
           group["project_source-"+this.loopIndex]=[''];
           group["hour-"+this.loopIndex]=["",[Validators.required,Validators.maxLength(2),Validators.max(11), Validators.pattern(/^-?(0|00|[1-9]\d*)?$/)]];
           group["min-"+this.loopIndex]=["",[Validators.required,Validators.maxLength(2),Validators.max(59), Validators.pattern(/^-?(0|00|[1-9]\d*)?$/)]];
           group["description-"+this.loopIndex]=[''];
           group["asset_path-"+this.loopIndex]=[''];
           group["preview_url-"+this.loopIndex]=[''];
     
     this.darEntryForm=this.fb.group(group)

  }
  getSortedDate(darDates){
    this.twoWeeksDates=[...darDates[0].cweek,...darDates[1].lastWeek];
    this.twoWeeksDates.sort((a,b)=>{return b.date-a.date})
   
  }
  saveDarEntry(){
    this.saveButton="Please wait...";
    let data=this.prepareDataformDatabase(this.darEntryForm.value);
    let date=data['date'];
    delete data['date'];
    let payload={...data};
    let onlyDate=date.split("-")[1];
  
    payload["hour"]=(parseInt(payload['hour'])*60)+parseInt(payload['min']);
    
    let dateOBj=this.getDateObject(onlyDate);
   // console.log(dateOBj)
    payload['project_id']=this.project.id;
    payload['emp_id']=this.getUserInfo().id;
    payload['manager_id']=this.project.managed_by;
    payload['dar_date']=new Date(date);
    payload["day"]=dateOBj[0].date;
    payload["dayName"]=dateOBj[0].day;
    payload["month"]=dateOBj[0].curr_month;
    payload["year"]=dateOBj[0].year;
    console.log(payload);

    this._darService.saveDarData(payload).subscribe(
      res=>{
          this.saveButton="Save";
          console.log(res);
      },
      err=>{
        this.saveButton="Save";
        console.log(err)
      }
     )
  }
  
  prepareDataformDatabase(data){
      let keys=Object.keys(data);
      let newOBj={};
      keys.forEach(key=>{
         newOBj[key.split("-")[0]]=data[key];
      })
      return newOBj;
  }

  getUserInfo(){
    let user=JSON.parse(localStorage.getItem("currentUser")).user.user[0];
    return user;
  }
  getDateObject(date){
      return this.twoWeeksDates.filter(element=>{return element.date==date});
  }
  getDarData(date,index){
  
    let dateObj=date.split("-");
    let payload={}
    payload["project_id"]=this.project.id;
    payload["emp_id"]=this.getUserInfo().id;
    payload["day"]=dateObj[1];
    payload["month"]=dateObj[0];
    payload["year"]=dateObj[2];

    this._darService.getDatewiseDarData(payload).subscribe(
      res=>{
        this.currentDar=res;
        this.selectedDateDar=this.currentDar.result.dars[0];
        this.formatDataForForm(this.selectedDateDar,index);
      },
      err=>{console.log(err)}
      )
    
  }
  /* this function takes two parameter 
   *  @ prams data - holding select date dar data 
   *  @param index - having selected selectbox row index value 
   * # the work of this function to attached index to all keys of Dar obeject
   * to set value in Dar selected row form
   */
  formatDataForForm(data,index){
        let payload={};
        if(typeof data!=undefined && typeof data!="undefined"){
          let resData:any;
          resData=this.formatHour(parseInt(data["hour"]));
          console.log(resData);
          payload['date-'+index]=data.month+"-"+data.day+"-"+data.year;
          payload['task_type-'+index]=data.task_type;
          payload['project_status-'+index]=data.project_status;
          payload['project_source-'+index]=data.project_source;
          payload['hour-'+index]=resData.hour;
          payload['min-'+index]=resData.min;
          payload['description-'+index]=data.description;
          payload['asset_path-'+index]=data.asset_path;
          payload['preview_url-'+index]=data.preview_url;
         // console.log(payload);
          this.darEntryForm.patchValue(payload)
        }else{
         this.darEntryForm.reset();
          payload['date-'+index]=""
          payload['task_type-'+index]="";
          payload['project_status-'+index]="";
          payload['project_source-'+index]="";
          payload['hour-'+index]="";
          payload['min-'+index]="";
          payload['description-'+index]="";
          payload['asset_path-'+index]="";
          payload['preview_url-'+index]="";
          this.darEntryForm.reset(payload)
        }
        
  }
  
  formatHour(time){
    //console.log(time)
      let sTime=(time*60);
      let rem=sTime%3600;
     // console.log(rem)
      let res={};
      let exact=0;
      let hour:any;
      let min=0;
            if(rem>0){
               exact=sTime-rem;
               hour=exact/3600;
               min=rem/60;
               hour= hour//+"."+min;
               res["hour"]=hour;
               res["min"]=min;
            }else{
               hour=sTime/3600;
               res["hour"]=hour;
               res["min"]=0;
            }
         return res;
 
   }


}
