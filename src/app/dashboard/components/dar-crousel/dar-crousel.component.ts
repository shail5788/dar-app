import { formatNumber } from '@angular/common';
import { Component, OnInit,Input,OnChanges } from '@angular/core';
import {FormGroup,FormControl, Validators} from "@angular/forms"
import {DarService} from '../../dar.service';
import { DashboardService } from '../../dashboard.service';
@Component({
  selector: 'app-dar-crousel',
  templateUrl: './dar-crousel.component.html',
  styleUrls: ['./dar-crousel.component.css']
})
export class DarCrouselComponent implements OnInit,OnChanges {
  @Input()darDates:any;
  @Input()project:any;
  @Input()activeProject;
  previousWeekDarForm:FormGroup;
  currentWeekDarForm:FormGroup;
  dateWise:FormGroup;
  startIndex=0;
  cformData;
  pformData;
  currentWeekButtom="Save";
  lastWeekButton="Save";
  constructor(private _darService:DarService,private _dashboardService:DashboardService) {
    this._dashboardService.projectSub.subscribe(res=>{
     // console.log(res);
      this.activeProject=res;
      this.getPreWeekDar(this.darDates[1].lastWeek);
      this.getCurrWeekDar(this.darDates[0].cweek);
     
    })
    setTimeout(() => { this.startIndex = 1; }, 1000);
   }
  days;
  ngOnInit(): void {
      let previousWeekField={};
      let currentWeekField={};
      this.darDates[1].lastWeek.forEach((element,index) => {
        previousWeekField["hour-"+element.date+"-"+this.project.id]=new FormControl("00",[Validators.required,Validators.maxLength(2),Validators.max(11)]),
        previousWeekField["min-"+element.date+"-"+this.project.id]=new FormControl("00",[Validators.required,Validators.maxLength(2),Validators.max(59)])
      });
      this.darDates[0].cweek.forEach(element=>{
  
        let currentDate=this.getCurrentDate()
        let obj={}
        if(parseInt(element.date)>currentDate){
           obj['value']="00";
           obj["disabled"]=true;
        }else{
          obj['value']="00";
           obj["disabled"]=false;
        }
    
        currentWeekField["hour-"+element.date+"-"+this.project.id]=new FormControl(obj,[Validators.required,Validators.maxLength(2),Validators.max(11)]);
        currentWeekField["min-"+element.date+"-"+this.project.id]=new FormControl(obj,[Validators.required,Validators.maxLength(2),Validators.max(59)]);
      })
      this.previousWeekDarForm=new FormGroup(previousWeekField)
      this.currentWeekDarForm=new FormGroup(currentWeekField);
      
    
  }
  
  ngOnChanges(){
    //console.log(this.activeProject)
      
  }
  savePreviousWeekDar(){
    this.lastWeekButton="wait...";
    let month=this.darDates[1].lastWeek[0].month;
    var data= this.prepareDataForDatabase(this.previousWeekDarForm.value,month)
    var newData=[...data].filter(element=>{return element!=undefined})
    this._darService.saveDarData(newData).subscribe(res=>{
      this.cformData=res;
     //this.currentWeekDarForm=this.cformData.result.dars;
       this.setLastWeekFormData(this.cformData.result.dars);
       this.lastWeekButton="Save";
     },err=>{//console.log(err)
    })
  }
  saveCurrentWeekDar(){
    let month=this.darDates[0].cweek[0].month;
    var data=this.prepareDataForDatabase(this.currentWeekDarForm.value,month)
    console.log(data);
    var newData=[...data].filter(element=>{
      //console.log(element)
      return element!=undefined})
   
    console.log(newData)
    this.currentWeekButtom="wait...";
    this._darService.saveDarData(newData).subscribe(res=>{
     this.cformData=res;
     console.log(this.cformData)
    //this.currentWeekDarForm=this.cformData.result.dars;
      this.setCurrentWeekFormData(this.cformData.result.dars);
      this.currentWeekButtom="save";
    },err=>{//console.log(err)
    })
  }
  getUser(){
    return JSON.parse(localStorage.getItem("currentUser")).user.user[0].id
  }
  setCurrentWeekFormData(data){
    console.log(data);
     this.currentWeekDarForm.patchValue(data);
  }
  setLastWeekFormData(data){

    this.previousWeekDarForm.patchValue(data);
  }
  prepareDataForDatabase(data,monthName){
     let dataSet=[];
    
    console.log(data);
    let month=new Date().getMonth()+1
    let year =new Date().getFullYear().toString();
    var keys=Object.keys(data);
      keys.forEach((key,i)=>{
         let splitedKeys= key.split('-')
         let day=splitedKeys[1];
         if (!(day in dataSet)){
            dataSet[day]={}
         }
         if(splitedKeys[0]=="hour"){
           dataSet[day]["hour"]=data[key];
           dataSet[day]["hour_form_field"]=key;
         }else{
          dataSet[day]["min"]=data[key];
          dataSet[day]["min_form_field"]=key;
         }
         dataSet[day]["day"]=day;
         dataSet[day]['project_id']=splitedKeys[2]
         dataSet[day]['month']=month.toString()
         dataSet[day]['year']=year.toString()
         dataSet[day]["emp_id"]=this.getUser();
       
         dataSet[day]["manager_id"]=this.activeProject.managed_by;
         dataSet[day]['dayName']=this.getDayName(day,monthName);
         let nmonth;
         let ndate;
          if(month<9){
            nmonth="0"+month;}else{nmonth=month;}
            if(parseInt(day)<9){
              ndate="0"+day;}else{ndate=day;}
         let darDate=month+"-"+day+"-"+year;
         let newDate=new Date(darDate);
         dataSet[day]['dar_date']=newDate;
      }) 
      // console.log(dataSet)
      return dataSet;
    
  }
  getDayName(date,monthName){

      let days=['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
      var crr= new Date()
      var year =crr.getFullYear();
      var month=monthName//this.darDates[0].cweek[0].month;
      var dateString=`${month},${year},${date}`;
      var dayName=new Date(dateString);
      return days[dayName.getDay()];
      
  }
  getPreWeekDar(preWeek){
   let lastWeek=preWeek;
   let months=['January' ,"Februray","March","April","May","June","July","Auguest","september","October","November","December"]
   let preDateArr=[];   
   lastWeek.forEach(element=>{
       
        element['monthNum']=months.indexOf(element.month)+1;
        element["emp_id"]=this.getUser();
        element["date"]=element.date;
        element["project_id"]=(typeof this.activeProject!=undefined && typeof this.activeProject!="undefined")?this.activeProject.id:null;
        preDateArr.push(element)
      }) 
     this._darService.getEmpPreAndCurrWeekDar(preDateArr).subscribe(res=>{
       this.pformData=res;
        //console.log(res);
        this.previousWeekDarForm.patchValue(this.pformData.result.dars);
     },err=>{//console.log(err)
    }) 
  }
  getCurrWeekDar(currWeek){
    let lastestWeek=currWeek;
    let months=['January' ,"Februray","March","April","May","June","July","Auguest","september","October","November","December"]
    let preDateArr=[];   
    lastestWeek.forEach(element=>{
        
         element['monthNum']=months.indexOf(element.month)+1;
         element["emp_id"]=this.getUser();
         element["date"]=element.date;
         element["project_id"]=(typeof this.activeProject!=undefined && typeof this.activeProject!="undefined")?this.activeProject.id:null;
         preDateArr.push(element)
       }) 
      this._darService.getEmpPreAndCurrWeekDar(preDateArr).subscribe(res=>{
        this.pformData=res;
        // console.log(res);
         this.currentWeekDarForm.patchValue(this.pformData.result.dars);
      },err=>{//console.log(err)
      }) 
  }
  getCurrentDate(){
      var today = new Date();
      var dd = String(today.getDate()).padStart(2, '0');
      var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
      var yyyy = today.getFullYear();

     // let date = mm + '/' + dd + '/' + yyyy;
      return  parseInt(dd);
      //document.write(today);
  }

}
