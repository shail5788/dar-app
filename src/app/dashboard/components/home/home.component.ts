import { Component, OnInit } from '@angular/core';
import {DashboardService} from '../../dashboard.service'
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  projectData;
  projects;
  error:object;
  darData;
  DarHoursRecord;
  notificationData;
  notifications;
  activitiyData;
  activities;
  constructor(private _dashboardService:DashboardService) { }

  ngOnInit(): void {
    this.getDarHoursData();
    this.getAssignedProject();
    this.getProjectDeliveryNotification();
    this.getAssignedActivity();
  }
    getAssignedProject(){
      let userID=this.getUserID();
      this._dashboardService.getAssignedProject(userID).subscribe(res=>{
       this.projectData=res;
      this.projects=this.projectData.result;
      
      },
      err=>{//console.log(err)
        if(typeof err.error.result!="undefined" && typeof err.error.result!=undefined){
           this.error=err.error.result.errors;
           
        }else{
          this.error=err.error;
        }
        
    
  //  console.log(this.error)
      })
  }

  getUserID(){
    var userData=JSON.parse(localStorage.getItem("currentUser")).user.user[0];
    return userData.id;
  }
  getDarHoursData(){
   let empid=this.getUserID();
   this._dashboardService.getDarHours(empid).subscribe(
     res=>{
      // console.log(res)
       this.darData=res;
       this.DarHoursRecord=this.darData.result;
     },
     err=>{console.log(err)}
     )
  }
  getProjectDeliveryNotification(){
    let emp_id=this.getUserID();
     this._dashboardService.getDeadLineNotification(emp_id).subscribe(
       res=>{
         this.notificationData=res;
         this.notifications=this.notificationData.result;
       },
       err=>{console.log(err)}
       )
  }
  
  /*Activity section */ 
  getAssignedActivity(){
    let userId=this.getUserID();
    this._dashboardService.getAssignedActivityToUser(userId).subscribe(
      res=>{
         this.activitiyData=res;
         this.activities=this.activitiyData.result;
         console.log(this.activities)
      },
      err=>{console.log(err)}
      )
  }

}
