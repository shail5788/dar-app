import { Component, OnInit,Input } from '@angular/core';
import { AccordionConfig } from 'ngx-bootstrap/accordion';
import { ProjectStatus } from '../../../models/projectStatus.model';
import { TaskTypeModel } from '../../../models/taskTypeModel';

import {DashboardService} from '../../dashboard.service'
// such override allows to keep some initial values
 
export function getAccordionConfig(): AccordionConfig {
  return Object.assign(new AccordionConfig(), { closeOthers: true });
}

@Component({
  selector: 'app-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.css'],
  providers: [{ provide: AccordionConfig, useFactory: getAccordionConfig }]

})
export class ProjectListComponent implements OnInit {
  customClass="customClass";
  @Input()projectList;
  darWeakwiseDates=[];
  openProject;
  days=[];
  months=[];
  curr;
  taskStatus:TaskTypeModel[]=[];
  tStatus;
  p_status:ProjectStatus[]=[]
  psdata;

  constructor(private _dashboardService:DashboardService) { }
    
  ngOnInit(): void {
    this.days=['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
    this.months=['January',"Februray","March","April","May","June","July","Auguest","september","October","November","December"];    
    this.curr=new Date(); 
    this.darWeakwiseDates[0]=this.getCurrentWeekDates();
    this.darWeakwiseDates[1]=this.getLastWeekDates();
    this.getStatus();
    this.getTaskType();
    //console.log(this.darWeakwiseDates);  
  }

  getCurrentWeekDates(){
      let currentWeek=[]
      let nextAndPrevDates:any={cweek:""};
      for (let i = 1; i <= 7; i++) {
          let calendarr={};
          let first = this.curr.getDate() - this.curr.getDay() + i
          let day = new Date(this.curr.setDate(first)).toISOString().slice(0, 10)
          let onlyDate=day.split("-")[2]
          calendarr['day']=this.days[this.curr.getDay()];
          calendarr['date']=parseInt(onlyDate);
          calendarr['month']=this.months[this.curr.getMonth()]
          calendarr['curr_month']=((this.curr.getMonth()+1)<=9)?"0"+(this.curr.getMonth()+1).toString():this.curr.getMonth()+1;
          calendarr['year']=this.curr.getFullYear().toString();
          currentWeek.push(calendarr);
          nextAndPrevDates.cweek=currentWeek;
        }
        return nextAndPrevDates;
  }
  getLastWeekDates(){
    let prevDates=[]
    let nextAndPrevDates:any={lastWeek:""};
    for(let j=0;j<=6;j++){
          let preDate={}
          var cur = new Date(); 
          var day = cur.getDate()-cur.getDay()-(6-j);
          var pdate = new Date(cur.setDate(day));
          preDate['day']=this.days[cur.getDay()]
          preDate['date']=parseInt(pdate.toISOString().slice(0,10).split("-")[2]);
          preDate['month']=this.months[cur.getMonth()]
          preDate['curr_month']=((this.curr.getMonth()+1)<=9)? "0"+(this.curr.getMonth()+1).toString():this.curr.getMonth()+1
          preDate['year']=cur.getFullYear().toString();
          prevDates.push(preDate)
          nextAndPrevDates.lastWeek=prevDates;
    }
    return nextAndPrevDates;
  }
  activeAccordion(project,event){
    console.log(event)
  
  }
  
  isOpenAccor(project){
    
   this.openProject=project;
   this._dashboardService.projectSub.next(this.openProject)
  }
  getStatus(){
    this._dashboardService.getStatus().subscribe(
      res=>{this.psdata=res;
         this.p_status=this.psdata.result
      },
      err=>{console.log(err)}
      )
  }
  getTaskType(){
        this._dashboardService.getTaskType().subscribe(
           res=>{
             this.tStatus=res;
             this.taskStatus=this.tStatus.result;
           },
           err=>{console.log(err)}
        )
  }
   
}
