import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http"

@Injectable({
  providedIn: 'root'
})
export class DarService {

  baseUrl="http://staging1.delivery-projects.com/dar-app-api/api/v1";
  constructor(private _http:HttpClient) { }
  saveDarData(darPayload){
    return this._http.post(`${this.baseUrl}/dar/save-dar`,{payload:darPayload}) 
  }
  getEmpPreAndCurrWeekDar(dates){
    return this._http.post(`${this.baseUrl}/dar/get-week-dar`,{payload:dates})
  }
  getDatewiseDarData(data){
    return this._http.post(`${this.baseUrl}/dar/get-dar`,{payload:data})
  }

}
