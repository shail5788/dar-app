import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http"
import { Subject } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class DashboardService {
  projectSub=new Subject<any>();
  activitySub=new Subject<any>();
  baseUrl="http://staging1.delivery-projects.com/dar-app-api/api/v1";
  constructor(private _http:HttpClient) { }
   
  getAssignedProject(userID){
    
    return this._http.get(`${this.baseUrl}/project/get-assigned-project-user/${userID}`);
  }
  getDarHours(empId){
    
    return this._http.get(`${this.baseUrl}/dar/working-day-calculation/${empId}`);
  }
  getDeadLineNotification(emp_id){
     return this._http.get(`${this.baseUrl}/dar/get-delivary-dates/${emp_id}`);
  }
 
   /*Activiry related code*/ 
  getAssignedActivityToUser(empid){
     return this._http.get(`${this.baseUrl}/activity/get-assigned-activity/${empid}`)
  }
  getTaskType(){
     return this._http.get(`${this.baseUrl}/project/get-task-type`);
  }
  getStatus(){
    return this._http.get(`${this.baseUrl}/project/get-status`)
  }






}
