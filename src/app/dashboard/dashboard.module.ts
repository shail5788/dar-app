import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule} from '@angular/router'
import { DashboardRoutingModule } from './dashboard-routing.module';
import {FormsModule,ReactiveFormsModule} from "@angular/forms";
import {HomeComponent} from './components/home/home.component';

import { DarTableComponent } from './components/dar-table/dar-table.component'
import {ProjectListComponent} from './components/project-list/project-list.component';
import {ShareModuleModule} from '../share-module/share-module.module';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import {DashboardService} from './dashboard.service';
import { DarCrouselComponent } from './components/dar-crousel/dar-crousel.component';
import { HomeWidgetComponent } from './components/home-widget/home-widget.component';
import { DeadlineNotificationComponent } from './components/deadline-notification/deadline-notification.component';
import { ActivityListComponent } from './components/activity-list/activity-list.component';
import { ActivityCrouselComponent } from './components/activity-crousel/activity-crousel.component';
import { DarEntryFormComponent } from './components/dar-entry-form/dar-entry-form.component';
@NgModule({
  declarations: [
    HomeComponent,
      DarTableComponent,
      ProjectListComponent,
      DarCrouselComponent,
      HomeWidgetComponent,
      DeadlineNotificationComponent,
      ActivityListComponent,
      ActivityCrouselComponent,
      DarEntryFormComponent
    ],
  imports: [
    CommonModule,
    RouterModule,
    DashboardRoutingModule,
    ShareModuleModule,
    ReactiveFormsModule,
    FormsModule,
    AccordionModule.forRoot(),
    CarouselModule.forRoot()
  ],
  providers:[DashboardService]
 
})
export class DashboardModule { }
