import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms'
import {AppRoutingModule} from './app-routing.module';
import { LoadingBarRouterModule } from '@ngx-loading-bar/router';
import { AppComponent } from './app.component';
import { DefaultComponent } from './layouts/default/default.component';
import { MainLayoutComponent } from './layouts/main-layout/main-layout.component';
import {ShareModuleModule} from './share-module/share-module.module';
import { ToastrModule } from "ng6-toastr-notifications";
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import {httpInterceptProvider} from './share-module/http-interceptor'
import {TokenInterceptorService} from './share-module/http-interceptor/token-interceptor.service';


@NgModule({
  declarations: [
    AppComponent,
    DefaultComponent,
    MainLayoutComponent

 
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ShareModuleModule,

    HttpClientModule,
    ToastrModule.forRoot(),
    LoadingBarRouterModule,
    BrowserAnimationsModule
  
  ],
  providers: [{provide:HTTP_INTERCEPTORS,useClass:TokenInterceptorService,multi:true}],
  bootstrap: [AppComponent]
})
export class AppModule { }
