import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AccountManagementComponent} from './components/account-management/account-management.component'
import { BrandManagementComponent } from './components/brand-management/brand-management.component';
import {SubrandManagementComponent} from './components/subrand-management/subrand-management.component'
const routes: Routes = [
         {path:"",redirectTo:"account-management"},
         {path:"account-management",component:AccountManagementComponent},
         {path:"brand",component:BrandManagementComponent},
         {path:"brand/:id",component:SubrandManagementComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountRoutingModule { }
