import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http"
import {environment} from './../../environments/environment'
import { Subject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class AccountService {
  // baseURl='http://staging1.delivery-projects.com/dar-app-api/api/v1/account';
  account= new Subject<any[]>();
  activeAccount=new Subject();
  brandModal=new Subject<any>();
  brands=new Subject<any>();
  constructor(private _http:HttpClient) { }
  createAccount(account){
     return this._http.post(`${environment.apiBaseUrl}/account/create-account`,{account})
  }
  createBrand(payload){
    return this._http.post(`${environment.apiBaseUrl}/account/create-brand`,{payload})
  }
  getAccounts(id){
     return this._http.get(`${environment.apiBaseUrl}/account/get-account/${id}`)
  }
  getAllAccounts(){
    return this._http.get(`${environment.apiBaseUrl}/account/get-account`)
  }
  getBrands(userID){
    return this._http.get(`${environment.apiBaseUrl}/account/${userID}/get-brand`);
  }
  getAllBrands(){
    return this._http.get(`${environment.apiBaseUrl}/account/get-all-brand`);
  }
  updateAccount(payload){
     return this._http.put(`${environment.apiBaseUrl}/account/update`,{payload})
  }
  updateBrand(payload){
    return this._http.put(`${environment.apiBaseUrl}/account/update-brand`,{payload})
  }
  createSubBrand(payload){
    return this._http.post(`${environment.apiBaseUrl}/brand/sub-brand`,{payload})
  }
  getSubBrand(brandId){
     return this._http.get(`${environment.apiBaseUrl}/brand/get-sub-brand/${brandId}`)
  }
}
