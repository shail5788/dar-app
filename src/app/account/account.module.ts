import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule,ReactiveFormsModule} from '@angular/forms'
import { AccountRoutingModule } from './account-routing.module';
import { AccountManagementComponent } from './components/account-management/account-management.component';
import {ShareModuleModule} from '../share-module/share-module.module';
import { AccTableComponent } from './components/acc-table/acc-table.component';
import { ModalPopupComponent } from './components/modal-popup/modal-popup.component';
import {AccountService} from './account.service';
import { BrandManagementComponent } from './components/brand-management/brand-management.component';
import { BrandTblComponent } from './components/brand-tbl/brand-tbl.component';
import { BrandModalComponent } from './components/brand-modal/brand-modal.component';
import { EditAccountModalComponent } from './components/edit-account-modal/edit-account-modal.component';
import { BrandEditModalComponent } from './components/brand-edit-modal/brand-edit-modal.component';
import { SubbrandModalComponent } from './components/subbrand-modal/subbrand-modal.component';
import { SubrandManagementComponent } from './components/subrand-management/subrand-management.component';
import { SubBrandTableComponent } from './components/sub-brand-table/sub-brand-table.component';

import { DataTablesModule } from 'angular-datatables';

@NgModule({
  declarations: [AccountManagementComponent, AccTableComponent, ModalPopupComponent, BrandManagementComponent, BrandTblComponent, BrandModalComponent, EditAccountModalComponent, BrandEditModalComponent, SubbrandModalComponent, SubrandManagementComponent, SubBrandTableComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    
    AccountRoutingModule,
    ShareModuleModule,
    DataTablesModule
  ],
  providers:[AccountService] 
  
})
export class AccountModule { }
