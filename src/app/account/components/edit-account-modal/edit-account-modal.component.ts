import { Component, OnInit,Input,OnChanges } from '@angular/core';
import{DesignUtilityService} from '../../../share-module/services/design-utility.service'
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {AccountService} from '../../account.service';
import { ToastrManager } from "ng6-toastr-notifications";
@Component({
  selector: 'app-edit-account-modal',
  templateUrl: './edit-account-modal.component.html',
  styleUrls: ['./edit-account-modal.component.css']
})
export class EditAccountModalComponent implements OnInit ,OnChanges{
  account;
  accounts;
  isModelClose=false;
@Input() currentAccount;
 constructor(private _designUtility:DesignUtilityService,private _accountService:AccountService,public _toastr:ToastrManager) {
     this._designUtility.editAccountPopup.subscribe(res=>{
        this.isModelClose=!res; 
     })
    
  }
 
 accountReactiveForm:FormGroup; 
 ngOnInit(): void {
   
   console.log(this.currentAccount)
   
   this.accountReactiveForm=new FormGroup({
      'account_name':new FormControl(null,Validators.required),
      'location':new FormControl(null,Validators.required)
   })
   this.accountReactiveForm.patchValue(this.currentAccount)
 }
 ngOnChanges(){
  if(this.accountReactiveForm){
    this.accountReactiveForm.patchValue(this.currentAccount)
  }
 
 }
 updateAccount(){
 
   //  console.log(this.accountReactiveForm)
     let id= JSON.parse(localStorage.getItem("currentUser")).user.user[0].id;
     let accountPayload={...this.accountReactiveForm.value};
     accountPayload.ac_managed_by=this.currentAccount.ac_managed_by;
     accountPayload.id=this.currentAccount.id;
     this._accountService.updateAccount(accountPayload).subscribe(res=>{
     this.account=res;
     this._toastr.successToastr("Account is updated successfully !","Success")
     console.log(this.account)
    
     this.getAccounts();
    },err=>{
      console.log(err)
    })
 }
 getAccounts(){
   let user= JSON.parse(localStorage.getItem("currentUser")).user.user[0];
   if(user.designation!="vp" && user.designation!="avp" ){
       this._accountService.getAccounts(user.id).subscribe(res=>{
       this.account=res;
       this._accountService.account.next(this.account);
     },err=>{
       console.log(err);
     })
   }else{
     this._accountService.getAllAccounts().subscribe(res=>{
        this.account=res;
        this._accountService.account.next(this.account);
     },err=>{
       console.log(err);
     })
   }
   
 }

 closeModel(){
     this.isModelClose=true;
     this._designUtility.editAccountPopup.next(false)
 }


}
