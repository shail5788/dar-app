import { Component, OnInit,Input } from '@angular/core';
import {DesignUtilityService} from '../../../share-module/services/design-utility.service';
import {AccountService}from '../../account.service';
@Component({
  selector: 'app-acc-table',
  templateUrl: './acc-table.component.html',
  styleUrls: ['./acc-table.component.css']
})
export class AccTableComponent implements OnInit {
  @Input() accounts;
    getActiveAccount;
   isOpenEditModal=false;
   dtOptions: DataTables.Settings = {};
  constructor(private _designUtility:DesignUtilityService,private _accountService:AccountService) {
    
   }
  
  ngOnInit(): void {
   console.log(this.accounts);
   this.dtOptions = {
    pagingType: 'numbers',
    pageLength: 10,
    processing: true
  };
  }
  openModel(event):void{
    
    this._designUtility.accountPopUp.next(true)
  }
  openEditModal(account){
    //console.log(account)
    this.isOpenEditModal=true;
    this.getActiveAccount=account;
   // this._accountService.activeAccount.next(account)
    this._designUtility.editAccountPopup.next(true)
  }
}
