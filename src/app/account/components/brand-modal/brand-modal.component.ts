import { Component, OnInit,Input } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import {DesignUtilityService} from './../../../share-module/services/design-utility.service'
import {AccountService} from './../../account.service'
import { ToastrManager } from "ng6-toastr-notifications";

@Component({
  selector: 'app-brand-modal',
  templateUrl: './brand-modal.component.html',
  styleUrls: ['./brand-modal.component.css']
})
export class BrandModalComponent implements OnInit {
  brandReactiveForm: FormGroup;
  isModelClose=false;
  accounts;
  accountData;
  constructor(private _designUitility:DesignUtilityService,private _accountService:AccountService,public _toastr:ToastrManager) { 
        this._designUitility.brandModal.subscribe(res=>{
            this.isModelClose= !res;
        })
  }

  ngOnInit(): void {
    this.brandReactiveForm=new FormGroup({
      'brand_title':new FormControl(null,Validators.required),
      'account_id':new FormControl(null,Validators.required)
   })
   var user=JSON.parse(localStorage.getItem("currentUser")).user.user[0];
   this.getAllAccount(user)
  
  }
  closeModel(){
    this.isModelClose=true;
    this._designUitility.brandModal.next(false)
  }
  addBrand(){
    console.log(this.brandReactiveForm.value)
    this._accountService.createBrand(this.brandReactiveForm.value).subscribe(
      res=>{console.log(res)
        this._toastr.successToastr("Breand added successfully !","Success")
      },
      err=>{console.log(err)
        this._toastr.errorToastr(err.error.result.errors.error,"Error");
        
      }
      )
  }
  getAllAccount(user){
    if(user.designation!="vp" && user.designation!="avp"){
            this._accountService.getAccounts(user.id).subscribe(res=>{
              this.accountData=res;
              this.accounts=this.accountData.result.account;
              
          },err=>{})
      }else{
              this._accountService.getAllAccounts().subscribe(res=>{
                this.accountData=res;
                this.accounts=this.accountData.result.account;
                
            },err=>{})
      }
  }
  getAllBrand(){
    var user=JSON.parse(localStorage.getItem("currentUser")).user.user[0];
    if(user.designation!="vp" && user.designation!="avp"){
        this._accountService.getBrands(user.id).subscribe(res=>{
          this._accountService.brands.next(res);
        },err=>{})
    }else{
      this._accountService.getAllBrands().subscribe(res=>{
        this._accountService.brands.next(res);
      },err=>{})
    }
  }
}
