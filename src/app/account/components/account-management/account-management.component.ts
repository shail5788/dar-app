import { Component, OnInit } from '@angular/core';
import {DesignUtilityService} from '../../../share-module/services/design-utility.service';
import {AccountService}from '../../account.service';
@Component({
  selector: 'app-account-management',
  templateUrl: './account-management.component.html',
  styleUrls: ['./account-management.component.css']
})
export class AccountManagementComponent implements OnInit {
  accountPop:any=false;
  accountsData
  constructor(private _designUitility:DesignUtilityService,private _accountService:AccountService) { 
      this._designUitility.accountPopUp.subscribe(res=>{
        this.accountPop=res;
        console.log(this.accountPop)
      })

      this._accountService.account.subscribe(res=>{
        this.accountsData=res;
      
      })
  }

  ngOnInit(): void {
    this.getAccounts()
  }
  getAccounts(){
    
    let user= JSON.parse(localStorage.getItem("currentUser")).user.user[0];
    if(user.designation!="vp" && user.designation!="avp"){
        this._accountService.getAccounts(user.id).subscribe(res=>{
          this.accountsData=res; 
        },err=>{
          console.log(err);
        })
    }else{
      this._accountService.getAllAccounts().subscribe(res=>{
        this.accountsData=res; 
      },err=>{
        console.log(err);
      })
    }
  }
}
