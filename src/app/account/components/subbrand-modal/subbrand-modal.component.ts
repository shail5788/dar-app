import { Component, OnInit,Input } from '@angular/core';
import { FormGroup,FormControl, Validators } from '@angular/forms';
import { ToastrManager } from 'ng6-toastr-notifications';
import { DesignUtilityService } from 'src/app/share-module/services/design-utility.service';
import { AccountService } from '../../account.service';

@Component({
  selector: 'app-subbrand-modal',
  templateUrl: './subbrand-modal.component.html',
  styleUrls: ['./subbrand-modal.component.css']
})
export class SubbrandModalComponent implements OnInit {
  @Input() brand;
  isModelClose=false;
  subBrandReactiveForm: FormGroup;
  constructor(private _designUitility:DesignUtilityService,private _accountService:AccountService,public _toastr:ToastrManager) { 
    this._designUitility.subBrandModal.subscribe(res=>{
        this.isModelClose= !res;
    })
}

  ngOnInit(): void {
    this.subBrandReactiveForm=new FormGroup({
      'title':new FormControl(null,Validators.required),
     })
    var user=JSON.parse(localStorage.getItem("currentUser")).user.user[0];
   //this.getAllAccount(user)
   //this.brandReactiveForm.patchValue(this.brand)
  }
  closeModel(){
    this.isModelClose=true;
    this._designUitility.subBrandModal.next(false)
  }
  addSubBrand(){
    let data={};  
    data={...this.subBrandReactiveForm.value};
    data['brand_id']=this.brand.id;
     this._accountService.createSubBrand(data).subscribe(
       res=>{
         console.log(res)
         this._toastr.successToastr("Sub-Brand Created successfully")
        },
       err=>{console.log(err)}
     )
  }
}
