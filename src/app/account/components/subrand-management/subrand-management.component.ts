import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AccountService } from '../../account.service';

@Component({
  selector: 'app-subrand-management',
  templateUrl: './subrand-management.component.html',
  styleUrls: ['./subrand-management.component.css']
})
export class SubrandManagementComponent implements OnInit {
  brandId;
  subBrands;
  subBrandData;
  constructor(private activatedRoute:ActivatedRoute,private _accountService:AccountService) {
    this.activatedRoute.params.subscribe(data=>{
      //console.log(data)
      this.brandId=data.id;
    })
   }

  ngOnInit(): void {
    console.log(this.brandId)
    let brandID=this.brandId
   this.getSubBrand(this.brandId) ;
  }
  getSubBrand(brandId){

      this._accountService.getSubBrand(brandId).subscribe(
        res=>{
          this.subBrandData=res;
          this.subBrands=this.subBrandData.result.sub_brand;
          //console.log(this.subBrands)
        },
        err=>{console.log(err)}
        )
  }
}
