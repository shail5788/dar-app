import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-sub-brand-table',
  templateUrl: './sub-brand-table.component.html',
  styleUrls: ['./sub-brand-table.component.css']
})
export class SubBrandTableComponent implements OnInit {
  @Input() subBrands
  dtOptions: DataTables.Settings = {};
  constructor() { }

  ngOnInit(): void { 
  //  console.log("SDfdf")
  this.dtOptions = {
    pagingType: 'numbers',
    pageLength: 10,
    processing: true
  };
    console.log(this.subBrands)
  }

}
