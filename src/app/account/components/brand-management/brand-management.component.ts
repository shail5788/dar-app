import { Component, OnInit } from '@angular/core';
import {AccountService} from '../../account.service';
import{DesignUtilityService} from "./../../../share-module/services/design-utility.service"
@Component({
  selector: 'app-brand-management',
  templateUrl: './brand-management.component.html',
  styleUrls: ['./brand-management.component.css']
})
export class BrandManagementComponent implements OnInit {
  brands;
  brandData;
  isBrandModalOpen=false;
  close=false;
  constructor(private _accountService:AccountService,private _designUtilityService:DesignUtilityService) { }

  ngOnInit(): void {
    let user= JSON.parse(localStorage.getItem("currentUser")).user.user[0];
      if(user.designation!="vp" && user.designation!="avp"){
        
          this._accountService.getBrands(user.id).subscribe(
            res=>{
              this.brandData=res;
              this.brands=this.brandData.result;
            },
            err=>{console.log(err)}
          )
      }else{
        this._accountService.getAllBrands().subscribe(
          res=>{
            this.brandData=res;
            this.brands=this.brandData.result;
          },
          err=>{console.log(err)}
        )
      }
    
  }
  openBrandModel(){
  
    this.isBrandModalOpen=true;
   
    this._designUtilityService.brandModal.next(true)
  }
}
