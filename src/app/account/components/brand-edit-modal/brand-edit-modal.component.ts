import { Component, OnInit,Input,OnChanges } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import {DesignUtilityService} from './../../../share-module/services/design-utility.service'
import {AccountService} from './../../account.service'
import { ToastrManager } from "ng6-toastr-notifications";
@Component({
  selector: 'app-brand-edit-modal',
  templateUrl: './brand-edit-modal.component.html',
  styleUrls: ['./brand-edit-modal.component.css']
})
export class BrandEditModalComponent implements OnInit,OnChanges {
  @Input()brand;
  //@Input()isModalOpen;
  brandReactiveForm: FormGroup;
  isModelClose=false;
  accounts;
  accountData;
  constructor(private _designUitility:DesignUtilityService,private _accountService:AccountService,public _toastr:ToastrManager) { 
    this._designUitility.editBrandModal.subscribe(res=>{
        this.isModelClose= !res;
    })
}

  
ngOnInit(): void {
  this.brandReactiveForm=new FormGroup({
    'brand_title':new FormControl(null,Validators.required),
    'account_id':new FormControl(null,Validators.required)
 })
 var user=JSON.parse(localStorage.getItem("currentUser")).user.user[0];
 this.getAllAccount(user)
 this.brandReactiveForm.patchValue(this.brand)

}
ngOnChanges(){
  if(this.brandReactiveForm){
    this.brandReactiveForm.patchValue(this.brand)
  }
}
closeModel(){
  this.isModelClose=true;
  this._designUitility.brandModal.next(false)
}
UpdateBrand(){
  console.log(this.brandReactiveForm.value)
  let payload={...this.brandReactiveForm.value};
  payload.id=this.brand.id;
  this._accountService.updateBrand(payload).subscribe(
    res=>{console.log(res)
      this.getAllBrand();
      this._toastr.successToastr("Breand updated successfully !","Success")
      this.closeModel();
    },
    err=>{console.log(err)
      this._toastr.errorToastr(err.error.result.errors.error,"Error");
      
    }
    )
}
getAllAccount(user){
  if(user.designation!="vp" && user.designation!="avp"){
          this._accountService.getAccounts(user.id).subscribe(res=>{
            this.accountData=res;
            this.accounts=this.accountData.result.account;
            
        },err=>{})
    }else{
            this._accountService.getAllAccounts().subscribe(res=>{
              this.accountData=res;
              this.accounts=this.accountData.result.account;
              
          },err=>{})
    }
}
getAllBrand(){
  var user=JSON.parse(localStorage.getItem("currentUser")).user.user[0];
  if(user.designation!="vp"&& user.designation!="avp"){
      this._accountService.getBrands(user.id).subscribe(res=>{
        this._accountService.brands.next(res);
      },err=>{})
  }else{
    this._accountService.getAllBrands().subscribe(res=>{
      this._accountService.brands.next(res);
    },err=>{})
  }
}

}
