import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit,Input } from '@angular/core';
import { DesignUtilityService } from 'src/app/share-module/services/design-utility.service';
import {AccountService} from './../../account.service';

@Component({
  selector: 'app-brand-tbl',
  templateUrl: './brand-tbl.component.html',
  styleUrls: ['./brand-tbl.component.css']
})
export class BrandTblComponent implements OnInit {
 @Input()brands;
 title:any;
brandList;
isModalOpen=false;
isSubbrandModelOpen=false;
activeBrand;
dtOptions: DataTables.Settings = {};
  constructor(private _accountService:AccountService,private _designUtility:DesignUtilityService) { 
      this._accountService.brands.subscribe(res=>{
       // console.log("kdjlfjdkl")
         this.brands=res.result;
         this.brandList=this.brands
         //console.log(this.brands)
      })
  }

  ngOnInit(): void {
    
    this.brandList=this.brands;
    this.dtOptions = {
      pagingType: 'numbers',
      pageLength: 10,
      processing: true
    };
   // console.log(this.brandList)
  }
  searchByTitle(){
    console.log(this.title)
    if(this.title==""){
      this.ngOnInit();
    }else{
      this.brandList= this.brandList.filter(res=>{return (res.brand_title.toLocaleLowerCase().match(this.title.toLocaleLowerCase())|| res.accountName.toLocaleLowerCase().match(this.title.toLocaleLowerCase()))})
    }
  }
  openEditModal(brand){
    // console.log(brand);
     this.activeBrand=brand;
     this.isModalOpen=true;
     this._designUtility.editBrandModal.next(true)
  }
  openSubBrandModal(brand){
    this.activeBrand=brand;
    this.isSubbrandModelOpen=true;
    this._designUtility.subBrandModal.next(true)
  }
}
