import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment'
import { env } from 'process';
@Injectable({
  providedIn: 'root'
})
export class UserService {
  baseUrl='http://staging1.delivery-projects.com/dar-app-api/api/v1/user';
  constructor(private _http:HttpClient) { }

  getAlluser(){
       return this._http.get(`${environment.apiBaseUrl}/user/get-users`)
  }
  getAllRoles(){
    return this._http.get(`${environment.apiBaseUrl}/user/get-roles`);
  }
  getAllModules(){
     return this._http.get(`${environment.apiBaseUrl}/user/get-modules`);
  }
  updateModule(data){
    return this._http.put(`${environment.apiBaseUrl}/user/update-modules`,{payload:data})
  }
}
