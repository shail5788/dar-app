import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {UserService} from './user.service';
import { UserRoutingModule } from './user-routing.module';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { FormsModule } from '@angular/forms'; 
import { ReactiveFormsModule } from '@angular/forms';
import { UserManagementComponent } from './components/user-management/user-management.component';
import { UserTableComponent } from './components/user-table/user-table.component';
  
import { DataTablesModule } from 'angular-datatables';

@NgModule({
  declarations: [UserManagementComponent, UserTableComponent],
  imports: [
    CommonModule,
    UserRoutingModule,
    DataTablesModule,
    FormsModule,
    ReactiveFormsModule,
    NgMultiSelectDropDownModule.forRoot()
  ],
  providers:[UserService]
})
export class UserModule { }
