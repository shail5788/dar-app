import { Component, Input, IterableDiffers, OnInit } from '@angular/core';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { ToastrManager } from 'ng6-toastr-notifications';
import { UserService } from '../../user.service';

@Component({
  selector: 'app-user-table',
  templateUrl: './user-table.component.html',
  styleUrls: ['./user-table.component.css']
})
export class UserTableComponent implements OnInit {
  @Input()userList;
  users;
  userLevel = [];
  dtOptions: DataTables.Settings = {};
  dropdownSettings:IDropdownSettings;
  roleDropdownSettings:IDropdownSettings;
  roles;
  modulesData;
  modules;
  constructor(private _userService:UserService,public _toastr:ToastrManager) { }

  ngOnInit(): void {
    this.getAllRoles();
    this.getAllModules();
    this.users=this.userList.result.user;
    this.dtOptions = {
      pagingType: 'numbers',
      pageLength: 10,
      processing: true
    };
    // this.userLevel=[
    //    {id:"1",name:"super-admin"},
    //    {id:"2",name:"admin"},
    //    {id:"3",name:"user"}
    // ]
      
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'id',
      textField: 'name',
      // selectAllText: 'Select All',
      // unSelectAllText: 'UnSelect All',
      itemsShowLimit: 5,
      allowSearchFilter: true
    };

       
    this.roleDropdownSettings = {
      singleSelection: true,
      idField: 'id',
      textField: 'name',
      // selectAllText: 'Select All',
      // unSelectAllText: 'UnSelect All',
      itemsShowLimit: 5,
      // allowSearchFilter: true
    };
     //console.log(this.userList)
  }
  onItemSelect(item: any,user) {
    // console.log(item);
    // console.log(user)
    let data={}
    data['user_id']=user.id;
    data['role']=user.role[0].id;
    data['access_module']=item.id;
    data["mode"]="subscribe";
    this._userService.updateModule(data).subscribe(
      res=>{console.log(res);
        this._toastr.successToastr("Module is activated for this employee");
      },
      err=>{
        console.log(err)
      }
      )
    
  }
  onSelectAll(items: any,user) {
    let data={}
    data['user_id']=user.id;
    data['role']=user.role;
    
    let moduleID=[];
    items.forEach(item=>{moduleID.push(item.id)})
    data['access_module']=moduleID.toString();
    data["mode"]="subscribe_all";
    this._userService.updateModule(data).subscribe(
      res=>{console.log(res);
        this._toastr.successToastr("Modules are activated for this employee");
      },
      err=>{
        console.log(err)
      }
      )
 
  }
  onItemDeSelect (item:any,user){
    let data={}
    data['user_id']=user.id;
    data['role']=user.role[0].id;
    data['access_module']=item.id;
    data["mode"]="un-subscribe";
    this._userService.updateModule(data).subscribe(
      res=>{console.log(res)
        this._toastr.successToastr("Module is deactivated for this employee");
      },
      err=>{
        console.log(err)
      }
      )
  }
  getAllRoles(){
      this._userService.getAllRoles().subscribe(res=>{
        this.roles=res;
        this.userLevel=this.roles.result;
      },err=>{console.log(err)})
  }
  onItemRoleSelect(event,user){
    console.log(event)
    console.log(user)
  }
  onRoleSelectAll(event,user){
    console.log(event)
    console.log(user)
  }
  onRoleItemDeSelect(event,user){
    console.log(event)
    console.log(user)
  }
  getAllModules(){
    this._userService.getAllModules().subscribe(
      res=>{
        console.log(res)
        this.modulesData=res;
        this.modules=this.modulesData.result;
      },err=>{console.log(err)})
  }

}
