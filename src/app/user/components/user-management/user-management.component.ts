import { Component, OnInit } from '@angular/core';
import { UserService } from '../../user.service';

@Component({
  selector: 'app-user-management',
  templateUrl: './user-management.component.html',
  styleUrls: ['./user-management.component.css']
})
export class UserManagementComponent implements OnInit {
   users;
  constructor(private _userService:UserService) { }

  ngOnInit(): void {
     this.getAllUser()
  }
  getAllUser(){
    this._userService.getAlluser().subscribe(
      res=>{this.users=res;},
      err=>{console.log(err)}
      )
  }

}
