import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import {FormGroup,FormControl, Validators, AsyncValidatorFn, ValidationErrors, AbstractControl} from '@angular/forms';
import {AuthServiceService} from '../auth-service.service';
import { ToastrManager } from "ng6-toastr-notifications";
import {UserModel}from '../../models/user.model'
import { Observable} from 'rxjs';
import {map} from 'rxjs/operators'
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
    currentUser:any;
    registerReactiveForm:FormGroup;
    buttonText="Sign Up"
  email;
  error;
  errorAlerts:any[]=[];
  dismissible = true;
  constructor(private _authService:AuthServiceService,private router:Router,public _toastr:ToastrManager) { }

  ngOnInit(): void {
    if(localStorage.getItem("currentUser")){
      this.router.navigate(['/dashboard'])
    }
    this.registerReactiveForm=new FormGroup({
      'name':new FormControl(null,Validators.required), 
      'email':new FormControl(null,[Validators.required,Validators.email],this.isCheckUser().bind(this)),
      'password':new FormControl(null,Validators.required),
      "designation":new FormControl(null,Validators.required)
    })
  }
  registerUser(){
    let user:UserModel;
    this.buttonText="Please wait...";
    user={...this.registerReactiveForm.value}
    user['access_level']="user";
    this._authService.registerUser(user).subscribe(res=>{
        
        this.currentUser=res;
        if(this.currentUser.status){
            localStorage.setItem("currentUser",JSON.stringify(this.currentUser.result))
            this._toastr.successToastr("Logged in successfully !","success")
            this.buttonText="Sign Up"
            setTimeout(()=>{
              this.router.navigate(["/dashboard"]);
            },1000)
        }
       

    },err=>{
         
           let error=err.error.result.errors.error;
           // this._toastr.errorToastr(error,"Opps");
           console.log(error)
           this.errorAlerts[0]=error;

            this.buttonText="Sign Up"
        
    })
    // console.log(this.registerReactiveForm.value)
  }
  isCheckUser(): AsyncValidatorFn {
    return (control: AbstractControl): Observable<{ [key: string]: any } | null> => {
       return this._authService.isEmailExist(control.value)
        .pipe(
           map(res => {
          this.email=res;
          console.log()
          // if username is already taken
          if (this.email.result.length>0) {
          //console.log(this.registerReactiveForm)
              return { 'emailexist': true};
          }
        })
      );
  };
}
onClosed(alert){}
}
