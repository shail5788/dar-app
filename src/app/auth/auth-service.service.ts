import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
@Injectable({
  providedIn: 'root'
})
export class AuthServiceService {

  constructor(private _http:HttpClient) { }
  baseUrl='http://staging1.delivery-projects.com/dar-app-api/api/v1/user';

  registerUser(user){
     return this._http.post(`${this.baseUrl}/register`,{user:user}); 
  }
  login(user){
     return this._http.post(`${this.baseUrl}/login`,{user})
  }
  isLogin(){
    if(localStorage.getItem("currentUser")){
        return true;
    }
    return false;
  }
  getLoggedInUser() {
    
    const loginUser = JSON.parse(localStorage.getItem("currentUser"));
    //console.log(loginUser);
    return loginUser;
  }
  isEmailExist(email){
     return this._http.get(`${this.baseUrl}/check-email/?email=${email}`);
  }
}
