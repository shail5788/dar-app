import {NgModule} from '@angular/core';
import {Routes,RouterModule} from '@angular/router';
import { ForgetPasswordComponent } from './forget-password/forget-password.component';
import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register.component';
const routes:Routes=[
    {path:'login',component:LoginComponent},
    {path:'', redirectTo:'/login',pathMatch:"full"},
    {path:"sign-up",component:RegisterComponent},
    {path:"forget-password",component:ForgetPasswordComponent}
]

@NgModule({
    imports:[RouterModule.forChild(routes)],
    exports:[RouterModule]
})

export class AuthRoutingModule{}
