import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {FormGroup,FormControl, Validators} from '@angular/forms';
import {AuthServiceService} from '../auth-service.service';
import { ToastrManager } from "ng6-toastr-notifications";
import { Observable } from 'rxjs';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  currentLoginUser;
  loginReactiveForm:FormGroup;
  buttonText="Sign In";
  error;
  errorAlerts:any[]=[];
  dismissible = true;
  email;
  constructor(private _authService:AuthServiceService,public _toaster:ToastrManager,private router:Router) { }

  ngOnInit(): void {
    if(localStorage.getItem("currentUser")){
      this.router.navigate(['/dashboard'])
    }
    this.loginReactiveForm=new FormGroup({
        'email':new FormControl(null,[Validators.required,Validators.email]),
        'password':new FormControl(null,Validators.required)
    })

  }
  login():void{
    this.buttonText="Please wait...";
      this._authService.login({...this.loginReactiveForm.value}).subscribe(res=>{
           console.log(res);
           this.currentLoginUser=res;
           if(this.currentLoginUser.response){
             localStorage.setItem("currentUser",JSON.stringify(this.currentLoginUser.result))
             this.buttonText="Sign In"
             this._toaster.successToastr("Logged In successfully !","Success")
             this.router.navigate(['/dashboard'])
           }
      },err=>{
        console.log(err.error.result)
        this.errorAlerts[0]=err.error.result;
        this.buttonText="Sign In"
      })
     // console.log(this.loginReactiveForm.value)
    
  }

  onClosed(alert){
    console.log(alert)
  }
 
}
