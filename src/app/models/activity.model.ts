export interface Activity {
    id:number,
    title:string,
    description:string,
    created_by:number,
    create_at?:string
}