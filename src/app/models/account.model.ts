export interface AccountModal {
    id:string,
    account_name:string,
    location:string,
    ac_managed_by:string,
    created_by:string
}