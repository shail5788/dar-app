export interface TaskTypeModel {
     id:string,
     title:string,
     description?:string
}