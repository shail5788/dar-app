export interface UserModel{
    name:string,
    email:string,
    password:string,
    designation:string
    profile_pic?:string
}