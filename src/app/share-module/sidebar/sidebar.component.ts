import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  constructor() { }
  isAdminAccess;
  
  isProjectManager;
  isAccessable; 
  darMenuText;
  userModule;
  accountModule;
  projectModule;
  dar;
  allModule=["user","account","project","dar","report"];
  loggedUser;
  ngOnInit(): void {
    this.userModule=false;
    this.isAccessable=false;
    this.isProjectManager=false;
    this.userModule=false;
    this.accountModule=false;
    this.dar=false;
    this.projectModule=false;
    var loggedInUser= JSON.parse(localStorage.getItem("currentUser"))
    this.loggedUser=loggedInUser;
    this.permissionWiseAccess(loggedInUser.user.user[0].role[0].name)
  }

   permissionWiseAccess(userType){
        switch(userType){
          case "super-admin":
            this.isAdminAccess=true;
            this.darMenuText="Emp Dar";
            break;
          case "admin":
            this.isProjectManager=true  
            this.darMenuText="Emp Dar"
            let module=this.loggedUser.user.user[0].modules;
            this.activatedModule(module);
            break;
          case 'user':
             this.isProjectManager=true  
             this.darMenuText="My Dar"
             let modules=this.loggedUser.user.user[0].modules;
             this.activatedModule(modules);
        
          default :
            this.darMenuText="My Dar"
            console.log("123")  
        }
   }
   activatedModule(modules){
        modules.forEach(item=>{
          if(item.name=="user"){
            console.log("tyutyutyutyu")
            this.userModule=true;
          }else if(item=="project"){
          this.projectModule=true;
          }else if(item=="account"){
            this.accountModule=true;
          }else if(item=="dar"){
            this.dar=true;
          }
      })
   }

}
