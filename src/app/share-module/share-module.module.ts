import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HeaderComponent} from './header/header.component';
import {FooterComponent} from './footer/footer.component';
import {SidebarComponent} from './sidebar/sidebar.component'
import {Routes,RouterModule} from '@angular/router';
import { RecordNotFoundComponent } from './record-not-found/record-not-found.component'

@NgModule({
  declarations: [HeaderComponent, FooterComponent, SidebarComponent, RecordNotFoundComponent],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports:[
    HeaderComponent, FooterComponent, SidebarComponent,RecordNotFoundComponent
  ],
  providers:[]
})
export class ShareModuleModule { }
