import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrManager } from "ng6-toastr-notifications"
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  toggleSideBar=false;
  backgroundColor;
  loggedInUserName;
  shortName;
  email;
  fontcolor;
  constructor(public _toastr:ToastrManager,private router:Router) { }

  ngOnInit(): void {
      this.backgroundColor=this.generateBackgroundColor();
      var currentUser=JSON.parse(localStorage.getItem("currentUser"));
      this.loggedInUserName=currentUser.user.user[0].name;
      this.email=currentUser.user.user[0].email;
      this.shortName=this.generateShortName(this.loggedInUserName);
      if(this.lightOrDark(this.backgroundColor)=="light"){
        this.fontcolor="black"
      }else{
        this.fontcolor="white"
      }
  }
  minMaxSideBar(event):void{
      this.toggleSideBar=!this.toggleSideBar;
      if(this.toggleSideBar){
        const body = document.getElementsByTagName('body')[0];
        body.classList.remove('sidebar-minified-out');
        body.classList.add('sidebar-minified');
      }else{
        const body = document.getElementsByTagName('body')[0];
        body.classList.remove('sidebar-minified');
        body.classList.add("sidebar-minified-out")
      }
  }
  generateBackgroundColor(){
      var letters = '0123456789ABCDEF';
      var color = '#';
      for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
      }
      return color;
  }
  generateShortName(name):string{
      
    if( name ) {
          var array = name.split(" ");
          switch ( array.length ) {
                case 1:
                  return array[0].charAt(0).toUpperCase();
                  break;
                default:
                  return array[0].charAt(0).toUpperCase() + array[ array.length -1 ].charAt(0).toUpperCase();
          }
    }
  }
    lightOrDark(color) {
             var r, g, b, hsp;
            // Check the format of the color, HEX or RGB?
            if (color.match(/^rgb/)) {
                // If RGB --> store the red, green, blue values in separate variables
                color = color.match(/^rgba?\((\d+),\s*(\d+),\s*(\d+)(?:,\s*(\d+(?:\.\d+)?))?\)$/);
                r = color[1];
                g = color[2];
                b = color[3];
            } 
            else {
                // If hex --> Convert it to RGB: http://gist.github.com/983661
                color = +("0x" + color.slice(1).replace( 
                color.length < 5 && /./g, '$&$&'));

                r = color >> 16;
                g = color >> 8 & 255;
                b = color & 255;
            }
             
            hsp = Math.sqrt(
            0.299 * (r * r) +
            0.587 * (g * g) +
            0.114 * (b * b)
            );
            if (hsp>127.5) {
              return 'light';
            } 
            else {
              return 'dark';
            }
  }
  logout(){

     localStorage.removeItem("currentUser");
     this._toastr.successToastr("logged Out successfully !","Success")
     setTimeout(()=>{
         this.router.navigate(["/login"])
     },1000)
  }

}
