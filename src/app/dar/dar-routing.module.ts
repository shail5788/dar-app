import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DarManagementComponent } from './components/dar-management/dar-management.component';


const routes: Routes = [
  {path:"",component:DarManagementComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DarRoutingModule { }
