import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http"
@Injectable({
  providedIn: 'root'
})
export class DarService {
  baseUrl="http://staging1.delivery-projects.com/dar-app-api/api/v1";
  constructor(private _http:HttpClient) { }
  getAssociatedUser(emp_id){
    return this._http.get(`${this.baseUrl}/dar/get-emps-associated-with-manager/${emp_id}`)
  }
  getAllEmployees(){
    return this._http.get(`${this.baseUrl}/dar/get-all-employees`);
  }
  getDarList(emp_id){
    return this._http.get(`${this.baseUrl}/dar/get-emp-dar/${emp_id}`);
  }
  getAllDarList(){
    return this._http.get(`${this.baseUrl}/dar/get-all-emp-dar`);
  }
  getSingleEmpDar(emp_id){
    return this._http.get(`${this.baseUrl}/dar/get-employee-dar/${emp_id}`);
  }
  darFilter(startDate,endDate,emp_id){
    return this._http.get(`${this.baseUrl}/dar/search/?startDate=${startDate}&endDate=${endDate}&emp_id=${emp_id}`);
  }
}
