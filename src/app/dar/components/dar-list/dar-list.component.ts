import { Component, OnInit,Input} from '@angular/core';

import { DatepickerDateCustomClasses } from 'ngx-bootstrap/datepicker';
import { DarService } from '../../dar.service';

@Component({
  selector: 'app-dar-list',
  templateUrl: './dar-list.component.html',
  styleUrls: ['./dar-list.component.css']
})
export class DarListComponent implements OnInit {
  @Input() DarData:any[];
  user;

  selectedCar: number;
  dar;
  dars;
  userList;
  userData;
  loading=true;
  constructor(private _darService:DarService) { 

    this.user=this.getUser();
    this.selectedCar=this.user.id;
    this.getDarData(this.selectedCar)
  }

  ngOnInit(): void {
    console.log(this.DarData)
   
    this.getAllUserAssociatedWithAManager(this.user.id)

    // this._darService.getAssociatedUser(this.user.id).subscribe(
    //   res=>{
    //     this.userData=res;
    //     this.userList=this.userData.result.employees;
    //   },
    //   err=>{console.log(err)}
    //   )

    
  }
  getUser(){
    return JSON.parse(localStorage.getItem("currentUser")).user.user[0];
  }
  selectedUser(event){
    console.log(this.selectedCar)
    if(this.selectedCar!=null){
      this.getDarData(this.selectedCar)
    }
    
  }
  getAllUserAssociatedWithAManager(emp_id){
    if(this.user.role[0].name=="admin"){
          this._darService.getAssociatedUser(emp_id).subscribe(
            res=>{
              this.userData=res;
              this.userList=this.userData.result.employees;
            },
            err=>{console.log(err)}
            )
    }else if(this.user.role[0].name=="super-admin" || this.user.role[0]=="admin"){
        this._darService.getAllEmployees().subscribe(
          res=>{
            this.userData=res;
            this.userList=this.userData.result.employees;
          },
          err=>{console.log(err)}
          )
    }else{
        this.getDarData(this.user.id);
    }
  
  }
  getDarData(empId){
     this._darService.getSingleEmpDar(empId).subscribe(
        res=>{
          this.dar=res;
          this.dars=this.dar.result.dars;
          this.loading=false;
        },
        err=>{console.log(err)}
        )
  }
  dateChange(event){
    let startDate;
    let endDate;
    console.log(event)
    if(event!=null){
      startDate=event[0].toLocaleDateString();
      endDate=event[1].toLocaleDateString();
      var year=event[0].getFullYear();
      var month=event[0].getMonth()+1 //getMonth is zero based;
      var day=event[0].getDate();
      var eyear=event[1].getFullYear();
      var emonth=event[1].getMonth()+1 //getMonth is zero based;
      var eday=event[1].getDate();
      startDate=year+"-"+month+"-"+day;
      endDate=eyear+"-"+emonth+"-"+eday;
      this._darService.darFilter(startDate,endDate,this.selectedCar).subscribe(
        res=>{
          this.dar=res; 
          this.dars=this.dar.result;
          console.log(this.dars)
        },
        err=>{console.log(err)}
        )
    }else{
      this.getDarData(this.selectedCar)
    }
   
   
}

}