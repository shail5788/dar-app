import { Component, OnInit } from '@angular/core';
import {DarService} from '../../dar.service';
@Component({
  selector: 'app-dar-management',
  templateUrl: './dar-management.component.html',
  styleUrls: ['./dar-management.component.css']
})
export class DarManagementComponent implements OnInit {
  darList;
  dars;
  user;
  constructor(private _darService:DarService) { }
  
 
  ngOnInit(): void {
      this.user=this.getUser() 
      console.log(this.user)
      if(this.user.designation=="account-manager" || this.user.designation=="project-manager"){
        this._darService.getDarList(this.user.id).subscribe(res=>{
            this.darList=res;
            this.dars=this.darList.result.dars;
            // console.log(this.dars);
        },err=>{console.log(err)})
      }else if(this.user.designation=="vp" || this.user.designation=="avp"){
        console.log("sdfjskdj")
            this._darService.getAllDarList().subscribe(res=>{
              this.darList=res;
              this.dars=this.darList.result.dars;
              // console.log(this.dars);
          },err=>{console.log(err)})
      }else{
        this._darService.getSingleEmpDar(this.user.id).subscribe(res=>{
           this.darList=res;
           this.dars=this.darList.result.dars;
          // console.log(this.dars)
        },err=>{})
      }
     
  }
  getUser(){
    return JSON.parse(localStorage.getItem("currentUser")).user.user[0];
  }

}
