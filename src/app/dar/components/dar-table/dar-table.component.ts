import { Component, OnInit,Input,TemplateRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { TaskTypeModel } from '../../../models/taskTypeModel';
import { DashboardService } from '../../../dashboard/dashboard.service';
import { ProjectStatus } from '../../../models/projectStatus.model';

@Component({
  selector: 'app-dar-table',
  templateUrl: './dar-table.component.html',
  styleUrls: ['./dar-table.component.css']
})
export class DarTableComponent implements OnInit {
  @Input()records;
  psdata
  p_status:ProjectStatus;
  user;
  tStatus;
  taskStatus:TaskTypeModel;
  monthsName=[]
  modalRef: BsModalRef;
  days=[];
  months=[];
  darWeakwiseDates=[];
  curr;
  activeRecord;
  constructor(private modalService: BsModalService,private _dashboardService:DashboardService) { }
  
  ngOnInit(): void {
    this.monthsName=["January","February","March","April","May","June","July","Auguest","September","October","November","December"]
    this.user=JSON.parse(localStorage.getItem("currentUser")).user.user[0];
    console.log(this.records)
        if(typeof this.records!=undefined && typeof this.records!="undefined"){
          this.records.forEach((item,index)=>{
            this.records[index]['hour']=this.formatHour(item.hour)
        })
        console.log(this.user)
      
    }
    this.days=['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
    this.months=['January',"Februray","March","April","May","June","July","Auguest","september","October","November","December"];    
    this.curr=new Date(); 
    this.darWeakwiseDates[0]=this.getCurrentWeekDates();
    this.darWeakwiseDates[1]=this.getLastWeekDates();
    this.getStatus();
    this.getTaskType();
  
  }

  formatHour(time){
   // console.log(time)
     let sTime=(time*60);
     let rem=sTime%3600;
     console.log(rem)
     let exact=0;
     let hour:any;
     let min=0;
           if(rem>0){
              exact=sTime-rem;
              hour=exact/3600;
              min=rem/60;
              hour= hour+"."+min;
           }else{
           	 hour=sTime/3600;
           }
    	  return hour;

  }
  openModal(template: TemplateRef<any>,record) {
    this.modalRef = this.modalService.show(template);
    this.activeRecord=record;
    this.modalRef.setClass('edit-dar-modal');
  }

  getStatus(){
    this._dashboardService.getStatus().subscribe(
      res=>{this.psdata=res;
         this.p_status=this.psdata.result
      },
      err=>{console.log(err)}
      )
  }
  getTaskType(){
        this._dashboardService.getTaskType().subscribe(
           res=>{
             this.tStatus=res;
             this.taskStatus=this.tStatus.result;
           },
           err=>{console.log(err)}
        )
  }
  getCurrentWeekDates(){
    let currentWeek=[]
    let nextAndPrevDates:any={cweek:""};
    for (let i = 1; i <= 7; i++) {
        let calendarr={};
        let first = this.curr.getDate() - this.curr.getDay() + i
        let day = new Date(this.curr.setDate(first)).toISOString().slice(0, 10)
        let onlyDate=day.split("-")[2]
        calendarr['day']=this.days[this.curr.getDay()];
        calendarr['date']=parseInt(onlyDate);
        calendarr['month']=this.months[this.curr.getMonth()]
        calendarr['curr_month']=((this.curr.getMonth()+1)<=9)?"0"+(this.curr.getMonth()+1).toString():this.curr.getMonth()+1;
        calendarr['year']=this.curr.getFullYear().toString();
        currentWeek.push(calendarr);
        nextAndPrevDates.cweek=currentWeek;
      }
      return nextAndPrevDates;
}
getLastWeekDates(){
  let prevDates=[]
  let nextAndPrevDates:any={lastWeek:""};
  for(let j=0;j<=6;j++){
        let preDate={}
        var cur = new Date(); 
        var day = cur.getDate()-cur.getDay()-(6-j);
        var pdate = new Date(cur.setDate(day));
        preDate['day']=this.days[cur.getDay()]
        preDate['date']=parseInt(pdate.toISOString().slice(0,10).split("-")[2]);
        preDate['month']=this.months[cur.getMonth()]
        preDate['curr_month']=((this.curr.getMonth()+1)<=9)? "0"+(this.curr.getMonth()+1).toString():this.curr.getMonth()+1
        preDate['year']=cur.getFullYear().toString();
        prevDates.push(preDate)
        nextAndPrevDates.lastWeek=prevDates;
  }
  return nextAndPrevDates;
}
  

}
