import { Component, Input, OnInit ,TemplateRef} from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import {FormBuilder ,FormControl,FormGroup, Validators} from "@angular/forms";
import { DashboardService } from '../../../dashboard/dashboard.service';
import { DarService } from '../../../dashboard/dar.service';
import { TaskTypeModel } from '../../../models/taskTypeModel';
import { ProjectStatus } from '../../../models/projectStatus.model';
@Component({
  selector: 'app-edit-dar',
  templateUrl: './edit-dar.component.html',
  styleUrls: ['./edit-dar.component.css']
})
export class EditDarComponent implements OnInit {
  @Input()modalRef;
  @Input() darDates;
  @Input() currentDar;
  @Input() taskType:TaskTypeModel;
  @Input() projectStatus:ProjectStatus;
  darEntryForm:FormGroup;
  saveButton="Save";
  twoWeeksDates
  constructor(private fb:FormBuilder,
    private _dashboardService:DashboardService,
    private _darService:DarService,
    private modalService: BsModalService
    
    ) { }

  ngOnInit(): void {
    let group ={};
    this.getSortedDate(this.darDates)
    group["date"]=['',Validators.required];
    group["task_type"]=['',Validators.required];
    group["project_status"]=['',Validators.required];
    group["project_source"]=[''];
    group["hour"]=["",[Validators.required,Validators.maxLength(2),Validators.max(11), Validators.pattern(/^-?(0|00|[1-9]\d*)?$/)]];
    group["min"]=["",[Validators.required,Validators.maxLength(2),Validators.max(59), Validators.pattern(/^-?(0|00|[1-9]\d*)?$/)]];
    group["description"]=[''];
    group["asset_path"]=[''];
    group["preview_url"]=[''];
    console.log(this.currentDar)
      this.darEntryForm=this.fb.group(group)
  }
  updateDarEntry(){

  }
  getSortedDate(darDates){
    this.twoWeeksDates=[...darDates[0].cweek,...darDates[1].lastWeek];
    this.twoWeeksDates.sort((a,b)=>{return b.date-a.date})
   
  }



}
