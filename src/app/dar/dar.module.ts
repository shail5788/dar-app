import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule} from'@angular/forms'
import { DarRoutingModule } from './dar-routing.module';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { DarManagementComponent } from './components/dar-management/dar-management.component';
import { DarListComponent } from './components/dar-list/dar-list.component';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { DarTableComponent } from './components/dar-table/dar-table.component';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { NgSelectModule } from '@ng-select/ng-select';
import { ModalModule } from 'ngx-bootstrap/modal';
import { EditDarComponent } from './components/edit-dar/edit-dar.component';
@NgModule({
  declarations: [DarManagementComponent, DarListComponent, DarTableComponent, EditDarComponent],
  imports: [
    CommonModule,
    DarRoutingModule,
    CarouselModule,
    FormsModule,
    ReactiveFormsModule,
    BsDatepickerModule.forRoot(),
    TabsModule.forRoot(),
    ModalModule.forRoot(),
    NgSelectModule
  ]
})
export class DarModule { }
