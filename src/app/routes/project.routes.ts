import {Routes} from '@angular/router';

export const PROJECT_ROUTES:Routes=[

   
     {
        path:"",
        loadChildren:()=>import('../project/project.module').then(m=>m.ProjectModule)
    }
]