import {Routes} from '@angular/router';

export const USER_ROUTER:Routes=[
     {path:"",loadChildren:()=>import('../user/user.module').then(m=>m.UserModule)}
]