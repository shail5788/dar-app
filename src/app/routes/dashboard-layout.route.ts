import {Routes} from '@angular/router';

export const DASHBOARD_LAYOUT_ROUTES:Routes=[

     {
         path:"",
         loadChildren:()=>import('../dashboard/dashboard.module').then(m=>m.DashboardModule)
     },
    
]